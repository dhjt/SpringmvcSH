# Changelog
<!-- @author DHJT 2018-09-04 -->

-------------------
##### 0.3.5

###### 新特性
- 增加工具jar [Hutool](http://www.hutool.cn/)
- 登录密码使用md5加密
- 增加了页面效果以及需要的图片以及样式

-------------------------------------------------------------------------------------------------------------

##### 0.3.4

###### 新特性
- 升级jar包
	+ log4j.version>2.11.1
	+ commons-logging.version>1.2
	+ c3p0.version>0.9.5.2
- ExtDemo登录页面以及登录功能
- 更改数据库连接，变更为mariadb

###### Bug修复
- 修复hibernate不能自动创建表的问题
	+ `hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect`

-------------------------------------------------------------------------------------------------------------

##### 0.3.3

###### 新特性
- Hibernate分页查询完善
- 加入Log4J的配置文件`log4j.properties`

-------------------------------------------------------------------------------------------------------------

##### 0.3.2

###### 新特性
- 添加了对返回json字符串的支持
- 页面接入后台数据
- 添加分页处理以及extjs分页列表显示页面

-------------------------------------------------------------------------------------------------------------

##### 0.3.1

###### 新特性
- 从README文档中拆分出版本说明为单独的文件；
- Extjs 实现首页、列表、手风琴模块；
- 添加toastr弹出框插件以及封装使用。

###### Bug修复
- 更正了0.3.0中描述错误的地方(issue#IMLCO by dhjt )

#### 0.3.0
- 升级版本	hibernate.version : 5.3.6.Final	spring.version : 5.0.8.RELEASE	jackson.version : 2.9.6
- 之前的spring与hibernate不兼容问题可能是maven下载jar包不完整导致，这次是jackson；
- 添加ExtJS 6.2的相关框架文件；

#### 0.2.0
- 集成Hibernate框架，hibernate.version : 5.1.14.Final；
	+ 为了与hibernate版本相兼容，降低spring版本 spring.version : 4.3.18.RELEASE；
	+ 简单的后台代码分层结构：Bean、Controller、Dao、Service；
	+ 使用hibernate简单的数据添加以及列表展示。

#### 0.1.0
- 建立一个使用SpringMVC+Spring的项目，可以处理请求以及返回相应展示的值。spring.version : 5.0.0.RELEASE