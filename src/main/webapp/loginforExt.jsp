<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.dhjt.web.Common.App"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	Calendar ca = Calendar.getInstance();
	String dataString = ca.get(Calendar.YEAR)+"";
	Object loginInfo = request.getAttribute("loginInfo");
	String errorInfo = "";
	if (loginInfo != null) {
		errorInfo = loginInfo.toString();
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>登录页面</title>
<!--[if lt IE 9]>
  <script src="index/html5shiv.js"></script>
<![endif]-->
<link type="text/css" rel="stylesheet" href="style/css/site.css">
<style>
body {
	background: url("style/images/BG.png") fixed center center no-repeat;
	background-size: cover;
	width: 100%;
	margin-left: 0;
	margin-right: 0;
}
.centerDiv {
	background: url("style/images/login.png") ;
	position: absolute;
 	left: 50%;
 	top: 50%;
	width:735px;
	height:458px;
	margin-left:-350px;
	margin-top:-200px;
}
</style>
</head>
<body  onkeypress="if (event.keyCode == 13) $('#loginBtn').click();">
	<form id="loginForm" method="POST">
		<div class="centerDiv">
			<div style="margin-top: 352px;margin-left: 245px;">
				<input style="height:30px;width: 190px;" id="username" name="username" value="" placeholder="请输入您的用户名" type="text" >
				<input style="margin-left: 15px;width:90px;height:30px;margin-top:-13px\9;" id="loginBtn" type="button" class="btn btn-lg btn-primary mgr10" value="登录" tabindex="1">
			</div>
			<div style="margin-top: 18px;margin-left: 245px;">
				<input style="height:30px;width: 190px;" id="password" name="password" value="" placeholder="请输入密码" type="password">
				<input style="margin-left: 15px;width:90px;height:30px;margin-top:-13px\9;" id="loginBtn1" type="button" class="btn btn-lg btn-primary mgr10" value="重置" tabindex="2">
			</div>
			<div style="margin-top: 10px;margin-left: 300px;">
				<input style="margin-left: 15px" id="remoberPassword" class="ui-checkbox" name="rememberMe" value="true" type="checkbox" tabIndex>
				<span>记住密码</span>
			</div>
			<div style="margin-top: 10px;margin-left: 195px;">
				<span class="error"></span>
			</div>
		</div>
	</form>
	<footer>
	<div class="footer">
		<div class="copyright">Copyright © 2015-<%=dataString%> by @DHJT All RightsReserved.</div>
	</div>
	</footer>
	<script type="text/javascript" src="JQuery/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="JQuery/jquery.validate.js"></script>
	<script type="text/javascript">
		function getCookie(name) {
			var strCookie = document.cookie;
			var arrCookie = strCookie.split("; ");
			for ( var i = 0; i < arrCookie.length; i++) {
				var arr = arrCookie[i].split("=");
				if (arr[0] == name)
					return arr[1];
			}
			return "";
		}
		$(document).ready(function() {
							//读取cookie的内容
							var username = getCookie('username');
							var password = getCookie('password');
							var remember = getCookie("remember");
							var errorInfo = '<%=errorInfo%>';

							if (errorInfo != '') {
								$("#loginForm").find(".error").html(errorInfo);
							}
							if (password && password.length == 2) {
								password = "";
							}
							$("#loginForm").find("#username").val(username);
							$("#loginForm").find("#password").val(password);
							if (remember == 'true') {
								$("#remoberPassword").attr("checked", true);
							} else {
								$("#remoberPassword").attr("checked", false);
							}

							$('#loginForm').find(':input').each(function() {
								var el = $(this);
								if (el.attr('id') && !el.val()) {
									el.focus();
									return false;
								}
							});

							$("#loginBtn").click(
											function() {
												var filter = /^([A-Za-z0-9_\.-]+)@([\dA-Za-z\.-]+)\.([A-Za-z\.]{2,6})$/;
												var username = $.trim($("#loginForm").find("#username").val());
												var password = $.trim($("#loginForm").find("#password").val());
												if (username == ""&& password == "") {
													$("#loginForm").find(".error").html("账号和密码均不能为空");
													$("#loginForm").find("#username");
													return false;
												}
												if (username == "") {
													$("#loginForm").find(".error").html("账号不能为空");
													$("#loginForm").find("#username");
													return false;
												}
												if (password == "") {
													$("#loginForm").find(".error").html("密码不能为空");
													$("#loginForm").find("#password");
													return false;
												}
												if (username.indexOf('@') >= 0) {
													if (!filter.test(username)) {
														$("#loginForm").find(".error").html("邮箱格式不正确");
														$("#loginForm").find("#username");
														return false;
													}
												}
												var url = "/SpringmvcSH/checkLogin";
   												$("#loginForm").attr("action", url);
   												$("#loginForm").attr("method", "post");
    											$("#loginForm").submit();
											});
			$("input[name='reset']").click(function() {
                $("#username").val('');
                $("#password").val('');
            });
			$('.qrcode-num').hover(
				function() {
					$('.wechat-qrcode').show('fast');
				}, function() {
					$('.wechat-qrcode').hide('fast');
				}).click(function() {
					return false;
				});
			});

	</script>

</body>
</html>