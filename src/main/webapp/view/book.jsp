<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="save.do" method="post">
    <fieldset style="width: 300px">
        <legend>添加书籍</legend>
        <table>
            <tr>
                <td align="right" style="font-weight: bold">书名</td>
                <td><input type="text" name="name" value="" style="width: 200px"></td>
            </tr>
            <tr>
                <td align="right" style="font-weight: bold">价格</td>
                <td><input type="text" name="price" value="" style="width: 200px"></td>
            </tr>
            <tr>
                <td align="right" style="font-weight: bold"></td>
                <td><input type="submit" name="add" value="添加" style="width: 200px"></td>
            </tr>
        </table>
    </fieldset>
</form>
<c:if test="${!empty listBooks}">
    <table border="1px solid #000000">
        <tr>
            <th width="80">Book ID</th>
            <th width="120">Book Name</th>
            <th width="120">Book Price</th>
        </tr>
        <c:forEach items="${listBooks}" var="book">
            <tr>
                <td>${book.bId}</td>
                <td>${book.name}</td>
                <td>${book.price}</td>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>