Ext.define("DH.index.LeftMenu", {
			extend : 'Ext.panel.Panel',
			layout : 'accordion', // 手风琴布局
			split : true,
			autoWidth : true,
			layoutConfig : {
				titleCollapse : false,
				animate : true,
				activeOnTop : true
			},
			config : {
				indexTabPanel : null
			},
			initComponent : function() {
				var _this = this;
				var items = [];
				var modelJump = Ext.create('DH.util.ModelJump');
				var menu = [{
					"children": [{
						"children": [],
						"comment": "",
						"icon": "dh-icon-button-shujudaoru",
						"id": "24",
						"sequence": 1,
						"title": "book检索",
						"type": "SJDL",
						"url": "DH.search.BasicQuery"
					}, {
						"children": [],
						"comment": "",
						"icon": "dh-icon-button-shujuyijiao",
						"id": "46",
						"sequence": 1,
						"title": "一体化检索",
						"type": "SJYJ",
						"url": "DH.search.IntegratedQuery"
					}],
					"comment": "",
					"icon": "dh-icon-button-shujuguanli",
					"id": "9",
					"sequence": 3,
					"title": "查询管理",
					"type": "SJGL",
					"url": ""
				}, {
			        "children": [{
		                "children": [],
		                "comment": "",
		                "icon": "dh-icon-button-shujudaoru",
		                "id": "24",
		                "sequence": 1,
		                "title": "数据导入",
		                "type": "SJDL",
		                "url": "DH.system.Log"
		            }, {
		                "children": [],
		                "comment": "",
		                "icon": "dh-icon-button-shujuyijiao",
		                "id": "46",
		                "sequence": 1,
		                "title": "数据移交",
		                "type": "SJYJ",
		                "url": "DH.data.MoveDataPanel"
		            }, {
		                "children": [],
		                "comment": "",
		                "icon": "dh-icon-button-shujuweihu",
		                "id": "25",
		                "sequence": 2,
		                "title": "数据维护",
		                "type": "SJWH",
		                "url": "DH.archive.ArchiveInput"
		            }, {
		                "children": [],
		                "comment": "",
		                "icon": "dh-icon-button-suoyinguanli",
		                "id": "26",
		                "sequence": 3,
		                "title": "索引管理",
		                "type": "SYGL",
		                "url": "DH.data.IndexManage"
		            }],
		        "comment": "",
		        "icon": "dh-icon-button-shujuguanli",
		        "id": "9",
		        "sequence": 4,
		        "title": "数据管理",
		        "type": "SJGL",
		        "url": ""
		    }, {
		        "children": [{
		                "children": [],
		                "comment": "",
		                "icon": "dh-icon-button-rizhiguanli",
		                "id": "31",
		                "sequence": 1,
		                "title": "日志管理",
		                "type": "RZGL",
		                "url": "DH.system.Log"
		            }, {
		                "children": [],
		                "comment": "",
		                "icon": "dh-icon-button-gongzuoliupeizhi",
		                "id": "49",
		                "sequence": 1,
		                "title": "工作流配置",
		                "type": "GZLPZ",
		                "url": "DH.borrow.WorkFlowView"
		            }, {
		                "children": [],
		                "comment": "工作流引擎配置",
		                "icon": "dh-icon-button-gongzuoliuqiyong",
		                "id": "48",
		                "sequence": 2,
		                "title": "工作流程管理",
		                "type": "GZLGL",
		                "url": "DH.borrow.WorkFlow"
		            }, {
		                "children": [],
		                "comment": "",
		                "icon": "dh-icon-button-bianmaweihu",
		                "id": "30",
		                "sequence": 4,
		                "title": "编码维护",
		                "type": "BMWH",
		                "url": "DH.system.CodingManage"
		            }, {
		                "children": [],
		                "comment": "",
		                "icon": "dh-icon-button-danganleixing",
		                "id": "34",
		                "sequence": 5,
		                "title": "报表设计",
		                "type": "BBSJ",
		                "url": "DH.bq.ManaGement"
		            }, {
		                "children": [],
		                "comment": "",
		                "icon": "",
		                "id": "51",
		                "sequence": 7,
		                "title": "按钮自定义",
		                "type": "ANDY",
		                "url": "DH.system.ButtonCustom"
		            }, {
		                "children": [],
		                "comment": "",
		                "icon": "dh-icon-button-jiemianzidingyi",
		                "id": "42",
		                "sequence": 8,
		                "title": "界面自定义",
		                "type": "",
		                "url": "DH.system.FromCustom"
		            }, {
		                "children": [],
		                "comment": "",
		                "icon": "dh-icon-button-shujuquanxianguanli",
		                "id": "45",
		                "sequence": 9,
		                "title": "数据权限管理",
		                "type": "SSQXGL",
		                "url": "DH.system.DataPurviewManage"
		            }, {
		                "children": [],
		                "comment": "导入导出功能",
		                "icon": "",
		                "id": "54",
		                "sequence": 12,
		                "title": "导入导出",
		                "type": "DRDC",
		                "url": "DH.system.ExportOrImport"
		            }, {
		                "children": [],
		                "comment": "系统初始化配置",
		                "icon": "",
		                "id": "59",
		                "sequence": 13,
		                "title": "系统配置",
		                "type": "XTPZ",
		                "url": "DH.system.Config"
		            }],
		        "comment": "",
		        "icon": "dh-icon-button-xitongguanli",
		        "id": "27",
		        "sequence": 6,
		        "title": "系统管理",
		        "type": "XTGL",
		        "url": ""
		    }];
				for (var i = 0; i < menu.length; i++) {
					var childernList = [];
					for (var j = 0; j < menu[i].children.length; j++) {
						childernList.push({
									text : menu[i].children[j].title,
									leaf : true,
									modelId : menu[i].children[j].id,
									sourcePath : menu[i].children[j].url,
									iconCls : menu[i].children[j].icon,
									modelType : menu[i].children[j].type //模块类型
								})
					}
					var store = Ext.create('Ext.data.TreeStore', {
								root : {
									expanded : true,
									children : childernList
								}
							});
					var tree = Ext.create('Ext.tree.Panel', {
								store : store,
								split : true,
								border : false,
								rootVisible : false,
								listeners : {
									itemclick : function(tree, event) {
										var modelName = event.data.text;
										var modelId = event.data.modelId;
										var sourcePath = event.data.sourcePath;
										var modelType = event.data.modelType; // 模块类型
										modelJump.addTabPanel(_this.indexTabPanel, modelName, modelId, sourcePath,modelType)
									}
								}
							});
					if (menu[i].children.length != 0) {// 一个子菜单都没就不加载
						items.push({
									layout : 'fit',
									title : menu[i].title,
									iconCls : menu[i].icon,
									items : tree
								})
					}
				}
				_this.items = items;
				this.callParent(arguments);
			},
			listeners : {
				render : function() {
					this.height = DH.Config.centerHeight;
				}
			}
		});