Ext.define('DH.index.charts.ColumnCharts', {
			extend : 'Ext.Panel',
			xtype : 'column-grouped-3d',
			layout : "fit",
			initComponent : function() {
				var me = this;
				var xField;
				var yField;
				var title;
				var archiveTypeIndex = 0;
				/**
				 * Ext所有主题列表
				 */
				var theme = ['muted', 'midnight', 'green', 'sky', 'red', 'purple', 'blue', 'yellow', 'category1', 'category2', 'category3', 'category4',
						'category5', 'category6'];
				Ext.Ajax.request({
							url : 'getColumnChartsField_ChartAction.action',
							async : false,
							success : function(response) {
								var data = Ext.decode(response.responseText);
								xField = data.xField;
								yField = data.yField;
								title = data.title;
							}
						});
				me.myDataStore = Ext.create('Ext.data.JsonStore', {
							autoLoad : true,
							fields : xField,
							proxy : {
								type : 'ajax',
								url : 'getColumnCharts_ChartAction.action'
							}
						});
				var number3D = Ext.create("Ext.chart.axis.Numeric3D", {
							position : 'left',
							fields : ['count'],
							grid : {
								odd : {
									fillStyle : 'rgba(255, 255, 255, 0.06)'
								},
								even : {
									fillStyle : 'rgba(0, 0, 0, 0.03)'
								}
							}
						});
				var bar3D = Ext.create("Ext.chart.series.Bar3D", {
							stacked : false,
							xField : ['nd'],
							yField : ['count'],
							highlight : true,
							label : {
								display : 'insideEnd',
								field : ['count'],
								renderer : function(text) {
								}
							}
						});
				var cartesian = Ext.create("Ext.chart.CartesianChart", {
							insetPadding : '20 5 0 5',
							theme : 'muted',
							interactions : ['itemhighlight'],
							store : me.myDataStore,
							legend : true,
							axes : [number3D, {
										type : 'category3d',
										position : 'bottom',
										fields : ['nd']
									}],
							series : bar3D
						})
				me.items = cartesian;
				/**
				 * 动态刷新数据，根据档案类型
				 */
				var task = Ext.TaskManager.start({
					run : function() {
						if (archiveTypeIndex == yField.length) {// 一圈结束，重置index
							archiveTypeIndex = 0;
						}
						bar3D.setTitle(title[archiveTypeIndex])
						me.myDataStore.getProxy().extraParams['className'] = yField[archiveTypeIndex];
						me.myDataStore.reload();
						var themeIndex = Math.round(Math.random() * theme.length);
						if (themeIndex >= theme.length) {
							themeIndex = theme.length - 1;
						}
						var tempTheme = theme[themeIndex]
						cartesian.setTheme(tempTheme)
						archiveTypeIndex++;
					},
					interval : 10000,// 刷新间隔
					repeat : 10000000
						// 循环次数
					});
				me.reloadTask = task;
				this.callParent(arguments);
			}
		});