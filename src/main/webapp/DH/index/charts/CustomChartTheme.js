Ext.define('Ext.chart.theme.Custom1', {
			extend : 'Ext.chart.theme.Base',
			singleton : true,
			alias : ['chart.theme.Custom1', 'chart.theme.custom1'],
			config : {
				colors : ['#115FA6', '#a61120', '#94AE0A', '#ffd13e', '#00ffff', '#24ad9a', '#7c7474', '#a66111']
			}
		});
Ext.define('Ext.chart.theme.Custom1Gradients', {
			extend : 'Ext.chart.theme.Base',
			singleton : true,
			alias : ['chart.theme.custom1-gradients', 'chart.theme.Custom1:gradients'],
			config : {
				colors : ['#115FA6', '#a61120', '#94AE0A', '#ffd13e', '#00ffff', '#24ad9a', '#7c7474', '#a66111'],
				gradients : {
					type : 'linear',
					degrees : 90
				}
			}
		});
