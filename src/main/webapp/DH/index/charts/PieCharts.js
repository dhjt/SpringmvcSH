Ext.define('DH.index.charts.PieCharts', {
			extend : 'Ext.Panel',
			xtype : 'pie-3d',
			layout : "fit",
			initComponent : function() {
				var me = this;
				var archiveTypeIndex = 0;
				var pieCharts = Ext.create("Ext.data.Model", {
							fields : ['yearCode', 'data']
						});
				var theme = ['category1', 'category2', 'category3', 'category4', 'category5', 'category6'];
				var archiveTypeStore = QR.Store.ArchiveTypeStore;
				this.myDataStore = Ext.create('Ext.data.Store', {
							autoLoad : true,
							model : pieCharts,
							proxy : {
								type : 'ajax',
								url : 'getPieCharts_ChartAction.action'
							}
						});
				var polarChart = Ext.create("Ext.chart.PolarChart", {
							theme : 'Custom1',
							innerPadding : 5,
							interactions : ['itemhighlight', 'rotatePie3d'],
							legend : {
								type : 'sprite',
								docked : 'bottom'
							},
							series : [{
										style : {
											opacity : 0.8,
											colorSpread : 0.8
										},
										type : 'pie3d',
										angleField : 'data',
										donut : 30,
										distortion : 0.8,// 角度
										highlight : {// 鼠标放置
											margin : 40
										},
										label : {
											field : 'yearCode'
										},
										tooltip : {
											trackMouse : true,
											renderer : function(tooltip, record, item) {
												tooltip.setHtml(record.get('yearCode') + '年: ' + record.get('data') + '%');
											}
										}
									}]
						})
				me.items = polarChart;
				var task = Ext.TaskManager.start({
					run : function() {
						if (archiveTypeIndex == archiveTypeStore.getCount()) {// 一圈结束，重置index
							archiveTypeIndex = 0;
						}
						me.myDataStore.getProxy().extraParams['className'] = archiveTypeStore.getAt(archiveTypeIndex).get('className');
						me.myDataStore.reload();
						var themeIndex = Math.round(Math.random() * theme.length);
						if (themeIndex >= theme.length) {
							themeIndex = theme.length - 1;
						}
						var tempTheme = theme[themeIndex]
						polarChart.setTheme(tempTheme)
						polarChart.setStore(me.myDataStore)
						archiveTypeIndex++;
					},
					interval : 10000,// 刷新间隔
					repeat : 10000000
						// 循环次数
					});
				me.reloadTask = task;
				this.callParent();
			}
		});