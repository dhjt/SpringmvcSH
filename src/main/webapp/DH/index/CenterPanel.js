Ext.define("DH.index.CenterPanel", {
	id : "indexPanel",
	extend : 'Ext.panel.Panel',
	layout : {
		type : 'hbox',
		pack : 'start',
		align : 'stretch'
	},
	border : false,
	config : {
		indexTabPanel : null
	},
	defaults : {
		frame : false,
		border : false,
		padding : 5
	},
	startTask : function() {
		if (this.columnChart) {
			Ext.TaskManager.start(this.columnChart.reloadTask);
		}
	},
	stopTask : function() {
		if (this.columnChart) {
			Ext.TaskManager.stop(this.columnChart.reloadTask);
		}
	},
	initComponent : function() {
		var _this = this;
//			_this.columnChart = Ext.create("DH.index.charts.ColumnCharts");
			//_this.pieChart = Ext.create("DH.index.charts.PieCharts");
			_this.items = [{
					layout : "border",
					frame : false,
					width : '32%',
					items : [{
								region : 'north',
								title : '工作台账',
								height : '50%',
								layout : 'fit',
								html : '111111111'
//								items : Ext.create("DH.index.LedgerPanel", {
//											indexTabPanel : _this.indexTabPanel
//										})
							}, {
								border : false,
								title : '服务器存储空间',
								region : 'center',
								layout : 'fit',
								html : '111111111'
//								items : Ext.create("DH.index.StorageInfo")
							}]
				}, {
					flex : 1,
					layout : "fit",
					title : '归档情况',
					items : _this.columnChart
				}];
		this.callParent(arguments);
	}
});