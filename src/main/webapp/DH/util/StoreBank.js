Ext.define("DH.util.StoreBank", {
			storeList : [],
			isLoadOver : false,
			initAllStore : function(flag) {
				var _this = this;
				this.modelTypeStore();
				// 数据统一加载
				this.storeLoadFactory(this.storeList[0], this.storeList, 0,flag);
			},
			/**
			 * 统一加载，不然容易造成加载延迟问题
			 * 
			 * @param {}
			 *            store
			 * @param {}
			 *            storeList
			 * @param {}
			 *            index
			 */
			storeLoadFactory : function(store, storeList, index,flag) {
				var _this = this;
				store.load({
							callback : function(records, options, success) {
								index++;
								if (storeList[index]) {
									_this.storeLoadFactory(storeList[index], storeList, index);
								} else {
									if (flag) {
										onPageLoad();
									} else {
										window.location.href = "common/4040.jsp";
									}
								}
							}
						})
			},
			modelTypeStore : function() {
				var store = Ext.create("Ext.data.Store", {
							fields : ['name', 'modelType'],
							data : [{
										name : "部门审核",
										modelType : "BMSH"
									}, {
										name : "数据维护",
										modelType : "SJWH"
									}]
						});
				DH.Store.ModelTypeStore = store;
				this.storeList.push(store);
//			},
//			/**
//			 * 所有coding的集合对象
//			 */
//			codingStores : function() {
//				var codingCM = Ext.create("Ext.data.Model", {
//							fields : ['id', 'name']
//						});
//				var codingItemCM = Ext.create("Ext.data.Model", {
//							fields : ['value', 'name']
//						});
//				var store = Ext.create('Ext.data.Store', {
//							model : codingCM,
//							proxy : {
//								type : 'ajax',
//								url : 'list_CodingAction.action'
//							},
//							listeners : {
//								load : function() {
//									store.each(function(rec) {
//												QR.Util.codings[rec.get("id")] = Ext.create('Ext.data.Store', {
//															model : codingItemCM,
//															proxy : {
//																type : 'ajax',
//																url : 'getItemsById_CodingAction.action',
//																extraParams : {
//																	id : rec.get("id")
//																}
//															}
//														}).load();
//											});
//								}
//							}
//						});
//				DH.Store.codingsStore = store;
//				this.storeList.push(store)
//			},
//			userStore : function() {
//				var _this = this;
//				var userCM = Ext.create("Ext.data.Model", {
//							fields : ['id', 'name']
//						});
//				var store = Ext.create('Ext.data.Store', {
//							model : userCM,
//							proxy : {
//								type : 'ajax',
//								url : 'getAllUser_OrginizationAction.action'
//							},
//							listeners : {
//								load : function() {
//									QR.Store.UserStore = store;
//								}
//							}
//						});
//				this.storeList.push(store)
			}
		});