Ext.define("DH.util.ModelJump", {
			alias : 'DH.util.ModelJump',
			config : {
				indexTabPanel : null,
				modelName : null,
				modelType : null
			},
			addTabPanel : function(indexTabPanel, modelName, modelId, sourcePath,modelType) {
				var newTab = indexTabPanel.getComponent(modelId);
				if (!newTab) {
					var text = "var newPanel = Ext.create('" + sourcePath + "',{modelType : '" + modelType + "'})";
					eval(text);
					newTab = indexTabPanel.add({
								id : modelId,
								title : modelName,
								modelType : modelType,
								closable : true,
								layout : "fit",
								items : [newPanel]
							});
					indexTabPanel.setActiveTab(newTab);
				} else { // 如果tab中存在，那么就直接将节点指向这个页面
					indexTabPanel.setActiveTab(newTab);
				}
			}
		});