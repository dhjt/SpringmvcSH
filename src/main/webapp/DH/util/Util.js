Ext.define("DH.util.Util", {
			codings : {},
			getActiveModelType : function(){
				return Ext.getCmp('CenterPanel').getActiveTab().modelType
			},
			downLoad : function(url, params) {
				if (params) {
					var submitForm = document.createElement("FORM");
					submitForm.target = 'frm_download';
					submitForm.action = url;
					document.body.appendChild(submitForm);
					submitForm.method = "POST";
					for (p in params) {
						if (!params[p] || !p)
							continue;
						var input = document.createElement('input');
						input.setAttribute('name', p);
						input.setAttribute('type', 'hidden');
						if (typeof(params[p]) == 'string') {
							input.setAttribute('value', params[p].replace(/\"/g, '\\"'));
						} else {
							input.setAttribute('value', params[p]);
						}
						submitForm.appendChild(input);
					}
					submitForm.submit();
					document.body.removeChild(submitForm);
				} else {
					var frame = document.getElementById('frm_download');
					frame.src = url;
				}
				
			},
			convertBean : function(bean, beanName) {
				beanName = beanName || 'bean';
				beanName = beanName + '.';
				var r = '{';
				for (p in bean) {
					var v = bean[p];
					if (v == null || v == undefined)
						v = '';
					if (typeof(v) == 'string') {
						if (r != '{')
							r += ',';
						r += '"' + beanName + p + '":"' + v.replace(/\"/g, '\\"') + '"';
					} else if (typeof(v) == 'number') {
						if (r != '{')
							r += ',';
						r += '"' + beanName + p + '":' + v
					} else if (typeof(v) == 'boolean') {
						if (r != '{')
							r += ',';
						r += '"' + beanName + p + '":' + v
					}
				}
				
				r += '}';
				var a = Ext.decode(r)
				return a
			},
			// 复写著录模块获取GridStore
			getGridJsonStore : function(url, fields, id) {
				return new Ext.data.JsonStore({
							autoLoad : false,
							id : id + "store",
							pageSize : 20,
							remoteSort : true,
							proxy : {
								type : 'ajax',
								url : url,
								reader : {
									// type : 'json',
									totalProperty : 'totalCount', // 数据条数
									root : 'content'
								}
							},
							fields : fields
						});
			},
			// 著录模块获取GridStore翻页信息
			getGridBBar : function(store) {
				return new Ext.PagingToolbar({
							store : store,
							displayInfo : true,
							emptyMsg : '没有数据可以显示'
						});
			},
			
			// //显示右下角提示消息
			// showToast : function (title,message){
			// Ext.ux.Toast.msg(title, message);
			// }
			showSuccessToast : function(content) {
				toastr.options = {
					"closeButton" : true,
					"debug" : false,
					"progressBar" : false,
					"positionClass" : "toast-bottom-right",
					"preventDuplicates" : false,
					"onclick" : null,
					"showDuration" : "300",
					"hideDuration" : "1000",
					"timeOut" : "3000",
					"extendedTimeOut" : "1000",
					"showEasing" : "swing",
					"hideEasing" : "linear",
					"showMethod" : "fadeIn",
					"hideMethod" : "fadeOut"
				}
				toastr.success(content, '成功')
			},
			submitFailure : function(form, action) {
				if (action.failureType == Ext.form.Action.SERVER_INVALID) {
					Ext.Msg.alert('提示', '保存失败!');
				} else {
					Ext.Msg.alert("错误", '无法访问后台！');
				}
			},
			showErrorToast : function(content) {
				toastr.options = {
					"closeButton" : true,
					"debug" : false,
					"progressBar" : false,
					"positionClass" : "toast-bottom-right",
					"preventDuplicates" : false,
					"onclick" : null,
					"showDuration" : "300",
					"hideDuration" : "1000",
					"timeOut" : "3000",
					"extendedTimeOut" : "1000",
					"showEasing" : "swing",
					"hideEasing" : "linear",
					"showMethod" : "fadeIn",
					"hideMethod" : "fadeOut"
				}
				toastr.error(content, '错误')
			}
		});