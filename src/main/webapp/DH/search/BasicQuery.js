/**
 * 基本查询平台
 */
Ext.define("DH.search.BasicQuery", {
	extend : 'Ext.grid.GridPanel',
	frame : false,
	columnLines : true, // 加上表格线
	store : null,
	bbar : {
		xtype : "pagingtoolbar",
		store : null,
		displayInfo : true
	},
	tbar : [{
		text : "浏览",
		handler : function() {
			var grid = this.up('grid');
//			grid.browseBorrow(grid.getBorrowNo(), grid.getWorkFlowID(), grid.getStatus());
		}
	},{
		text : "借阅单",
		handler : function() {
		}
	}, "->", {
		xtype : "combo",
		fieldLabel : "借阅状态",
		queryMode : 'local',
		displayField : 'name',
		valueField : 'id',
		value : "all",
		store : null,
		listeners : {
			select : function(obj) {
			}
		}
	}],
	selModel : Ext.create("Ext.selection.CheckboxModel", {
		checkOnly : false
	}),
	listeners : {
		'itemdblclick' : function(view, record, item, index, event, eOpts) {
			alert(121);
		}
	},
	initComponent : function() {
		var _this = this;
		var cm = Ext.create("Ext.data.Model", {
			fields : [{
						name : "id"
					}, {
						name : "name"
					}, {
						name : "price"
					}]
		});
		var store = Ext.create("Ext.data.Store", {
					model : cm,
					autoLoad : true,
					pageSize : 10,
					proxy : {
						type : "ajax",
						actionMethods : {
							create : 'POST',
							read : 'POST', // by default GET
							update : 'POST',
							destroy : 'POST'
						},
						url : "/SpringmvcSH/book/getBookPage",
						reader : {
							root : "content",
							totalProperty : 'totalCount'
						}
					}
				});
		this.store = store;
		this.bbar.store = store;
		var columns = [ {
			text : 'id',
			dataIndex : 'bId',
			sortable : true,
			align : 'center',
			width : '10%'
		}, {
			header : '名称',
			dataIndex : "name",
			sortable : true,
			width : "40%",
			align : 'center',
			stripeRows : true,
			enableDragDrop : true
		}, {
			header : '价格',
			dataIndex : "price",
			sortable : true,
			width : "15%",
			align : 'left',
			stripeRows : true,
			enableDragDrop : true
		}, {
			header : '价格',
			dataIndex : "bz",
			sortable : true,
			width : "30%",
			align : 'left',
			stripeRows : true,
			enableDragDrop : true
		}];
		_this.columns = columns;
		this.callParent(arguments);
	},
	
})