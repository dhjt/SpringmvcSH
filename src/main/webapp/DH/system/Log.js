//Ext.define("DH.system.Log", {
//	extend : 'Ext.panel.Panel',
//	config : {
//	},
//	html : '111111111111111111111'
//});
Ext.define("DH.system.Log", {
	extend : 'Ext.grid.GridPanel',
	frame : false,
	columnLines : true, // 加上表格线
	store : null,
	old : 'TT', // 用于存放之前查询条件
	initComponent : function() {
		var _this = this;
		_this.selModel = Ext.create("Ext.selection.CheckboxModel", {
			checkOnly : false
		});
		var gridPlugins = [{
			ptype : 'rowexpander',
			rowBodyTpl : new Ext.XTemplate('<p><b>具体数据:</b> {title}</p><br>', {
						formatChange : function(v) {
							var color = v >= 0 ? 'green' : 'red';
							return '<span style="color: ' + color + ';">' + Ext.util.Format.usMoney(v) + '</span>';
						}
					})
		}];
		_this.plugins = gridPlugins;
		DH.Util.showSuccessToast("页面显示成功！");
		DH.Util.showErrorToast("页面显示成功！");
		this.initStore();
		this.initTbar(_this);
		this.callParent(arguments);
	},
	columns : [ {
		text : 'id',
		dataIndex : 'id',
		sortable : true,
		align : 'center',
		width : '10%'
	}, {
		header : '题名',
		dataIndex : "title",
		sortable : true,
		width : "45%",
		align : 'left',
		stripeRows : true,
		enableDragDrop : true
	}, {
		header : '年度',
		dataIndex : "yearCode",
		sortable : true,
		width : "10%",
		align : 'center',
		stripeRows : true,
		enableDragDrop : true
	}, {
		header : '页数',
		dataIndex : "pageCount",
		sortable : true,
		width : "5%",
		align : 'center',
		stripeRows : true,
		enableDragDrop : true
	}, {
		header : 'int1',
		dataIndex : "int1",
		sortable : true,
		width : "10%",
		align : 'center',
		stripeRows : true,
		enableDragDrop : true
	}, {
		header : 'int12',
		dataIndex : "int12",
		sortable : true,
		width : "10%",
		align : 'center',
		stripeRows : true
	}, {
		header : '操作',
		dataIndex : 'docExist',
		locked : true,
		sortable : true,
		align : 'center',
		width : 200,
		renderer : function(value) {
			if (value === 'y') {
				return "<img src='style/images/ck.gif'/>"
			} else {
				return ""
			}
		}
	} ],
	features : [ {
		id : 'group',
		ftype : 'grouping',
		startCollapsed : true,
		groupHeaderTpl : '{name}({rows.length})'
	} ],
	tbar : null,
	initStore : function() {
		var store = Ext.create('Ext.data.Store', {
			model : Ext.create("Ext.data.Model", {
				fields : [ 'id', 'ref', 'title', 'archiveTypeId', 'ljbm', 'fondsCode', 'createUser', 'cretaeDate', 'archiveTypeName',
					'modiyUser', 'modiyDate', 'isDelete', 'nd', 'type' ]
			}),
			proxy : {
				type : "ajax",
				timeout : 100000000,
				actionMethods : {
					create : 'POST',
					read : 'POST', // by default GET
					update : 'POST',
					destroy : 'POST'
				},
				url : "query_LuceneAction.action"
			},
			groupField : 'yearCode',
			groupDir : "desc",
			listeners : {
				load : function() {
					if (store.getCount() >= 1000) {
						Ext.Msg.alert('提示', '检索数据已经超过1000条,请再结果中检索');
					}
				}
			}
		});
		this.store = store;
	},
	initTbar : function(_this) {
		var tbar = [ '<div style="font-size:14px;color:red;" > <a> 请 输 入 查 询 条 件 ：</a></div>', {
			id : "luceneQuery",
			xtype : "textfield",
			layout : 'center',
			width : 400,
			height : 40,
			style : 'text-align:center',
			allowBlank : true
		}, {
			boxLabel : '精确检索',
			xtype : "checkbox",
			width : 100,
			border : true
		}, {
			text : '搜索',
			scope : this,
			width : 100,
			height : 40,
			handler : function() {
				_this.old = 'TT'; // 先清空OLD信息
				if (Ext.getCmp('luceneQuery').getValue() == '') {
					Ext.Msg.alert('提示', '条件不能为空');
					return;
				}
				var luncenetext = Ext.getCmp('luceneQuery').getValue();
				if (_this.down('checkbox').getValue()) {
					luncenetext = '"' + luncenetext + '"';
				}
				_this.getStore().getProxy().url = "query_LuceneAction.action";
				_this.getStore().getProxy().extraParams = {
					luncenetext : luncenetext,
					modelType : _this.modelType
				};
				_this.getStore().reload();
				_this.old = luncenetext;
			}
		}, {
			text : '从结果中搜索',
			scope : this,
			width : 100,
			height : 40,
			handler : function() {
				var luncenetext = Ext.getCmp('luceneQuery').getValue();
				if (_this.down('checkbox').getValue()) {
					luncenetext = '"' + luncenetext + '"';
				}
				luncenetext = _this.old + "+" + luncenetext;
				if (Ext.getCmp('luceneQuery').getValue() == '') {
					Ext.Msg.alert('提示', '条件不能为空');
					return;
				}
				_this.getStore().getProxy().url = "query_LuceneAction.action";
				_this.getStore().getProxy().extraParams = {
					luncenetext : luncenetext,
					modelType : _this.modelType
				};
				_this.getStore().reload();
				_this.old = luncenetext;
			}
		}, "->", {
			text : '启用/关闭年度分组',
			scope : this,
			width : 150,
			height : 40,
			handler : function() {
				var store = _this.store;
				if (store.isGrouped()) {
					store.setGroupField('');
				} else {
					store.setGroupField('yearCode');
				}
			}
		} ]
		this.tbar = tbar;
	},
	listeners : {
		cellclick : function(grid, td, cellIndex, record, tr, rowIndex, e) {
			// 由于EXT锁定列GRID和非锁定列GRID为2个GRID，导致列号重复，新增图片路径判断
			var model = grid.getSelectionModel();
			var type = model.selected.items[model.selected.length - 1].data.archiveType;
			var className = model.selected.items[model.selected.length - 1].data.className;
			if (td.innerHTML.indexOf("ck.gif") <= -1) {
				return;
			}
			var gg = Ext.create("QR.view.ArchiveViewPanel", {
				ownerId : model.selected.items[model.selected.length - 1].data.id,
				archiveTypeLevel : type,
				className : className,
				modelType : 'YTH',
//				url : 'getFolderTree_ArchiveQuery.action'
			});

			var dialog = Ext.create("QR.util.Dialog", {
				title : "浏览",
				maximized : true,
				items : gg,
				buttons : [ {
					xtype : 'button',
					text : '关闭',
					handler : function() {
						dialog.close();
					}
				} ]
			});
			dialog.show();
		}
	},
	getSelectIds : function() {
		var ids = '';
		var v = this.getSelectionModel().getSelection();
		if (v.length > 0) {
			for (var i = 0; i < v.length; i++) {
				ids += ',' + v[i].id;
			}
			ids = ids.substring(1);
		} else {
			ids = '';
		}
		return ids;
	},
	getSelectId : function() {
		return this.getSelectionModel().getSelection()[0].id;
	},
	getSelectTitles : function() {
		var titles = '';
		var v = this.getSelectionModel().getSelection();
		if (v.length > 0) {
			for (var i = 0; i < v.length; i++) {
				titles += ',' + v[i].get('title');
			}
			titles = titles.substring(1);
		} else {
			titles = '';
		}
		titles = titles.replace(new RegExp("<font color='red'>", "gm"), "");
		titles = titles.replace(new RegExp("</font>", "gm"), "");
		return titles;
	},
});