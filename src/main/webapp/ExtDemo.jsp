<%-- <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> --%>

<%@ page language="java" import="java.util.*"
	errorPage="/common/SessionTimeout2.jsp" pageEncoding="UTF-8"
	contentType="text/html; charset=UTF-8"%>
<%@ page import="com.dhjt.web.Common.App"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String baseAppName = request.getServerName();
	String themeName = App.getThemeName();
	String fontSize = App.getFontSize();
	String lineCount = App.getLineCount();
	String fontFamily = App.getFontFamily();
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>SpringmvcSH--DHJT</title>
</head>
<style>
* {
	font-family: '<%=fontFamily%>';
	font-size: <%=fontSize%>px;
}

#loading {
	position: absolute;
	left: 40%;
	top: 40%;
	padding: 2px;
	z-index: 20001;
	height: auto;
}

#loading-msg {
	font: normal 10px arial, tahoma, sans-serif;
}

#loading .loading-indicator {
	background: white;
	color: #444;
	font: bold 13px tahoma, arial, helvetica;
	padding: 10px;
	margin: 0;
	height: auto;
}

.topBG {
	height: 80px;
	width:100%;
  	background-image: url('style/images/Logo_BG.png');
}
</style>
<body>
	<iframe name="frm_download" id="frm_download" src="" frameborder="0" style="display: none;"></iframe>
	<div id="loading">
		<div class="loading-indicator">
			<img src="style/images/loding-logo.gif" width="32" height="32"
				style="margin-right: 8px; float: left; vertical-align: top;" /> <span>1212</span><br />
			<span id="loading-msg">加载基础架构中...</span>
		</div>
	</div>
</body>
<!-- 导入EXTJS、JQuery基础包 -->
<script src="ext/ext-all.js"></script>
<script src="JQuery/jquery-1.11.1.min.js"></script>
<script type="text/javascript">document.getElementById('loading-msg').innerHTML = '加载中文语言包......';</script>
<!-- 导入中文JS包 -->
<script src="ext/packages/locale/overrides/zh_CN/ext-locale-zh_CN.js"></script>
<!-- 左下角提示框 -->
<script src="toastr/toastr.min.js"></script>
<link type="text/css" href="toastr/toastr.min.css" rel="stylesheet" />
<script type="text/javascript">document.getElementById('loading-msg').innerHTML = '加载界面样式中......';</script>
<!-- 导入自定义CSS-->

<!-- EXT基础样式包 -->
<link type="text/css" href="ext/packages/theme-<%=themeName%>/resources/theme-<%=themeName%>-all.css" rel="stylesheet" />
<!-- 图表JS、CSS -->
<script src="ext/packages/charts/classic/charts.js"></script>
<link type="text/css" href="ext/packages/charts/classic/<%=themeName%>/resources/charts-all.css" rel="stylesheet" />
<!-- EXT UX组件 -->
<script src="ext/packages/ux/build/ux-debug.js"></script>
<link type="text/css" href="ext/packages/ux/build/<%=themeName%>/resources/ux-all.css" rel="stylesheet" />
<!-- 自定义表报主题 -->

<!-- EXT Pivot -->
<script src="ext/ExportAll1.0.js"></script>
<script src="ext/pivot-all.js"></script>

<link type="text/css" href="ext/resources/pivot-all.css" rel="stylesheet" />
<script type="text/javascript">
(function() {
	Ext.Loader.setConfig({
		enabled : true
	});
});
Ext.namespace('DH.Config', 'DH.User', 'DH.Store', 'DH.Util', 'DH.Constant', 'DH.index');
DH.User.ID = '1';
DH.User.Name = '系统管理员';
DH.User.OrgID = '1';
DH.User.OrgName = 'DHJT';
DH.User.PId = '';
DH.Config.BasePath = '<%=basePath%>';
// Ext.Ajax.on('requestcomplete', function(conn, response, options) {
// 	if (response.getAllResponseHeaders) { //如果response的headers存在
// 		if (typeof response.getAllResponseHeaders().sessionstatus != 'undefined') { //session超时了
// 			window.location.href = "common/SessionTimeout2.jsp";
// 		}
// 	}
// }, this);
Ext.Ajax.timeout = 180000;
DH.Util = Ext.create("DH.util.Util");
Ext.create("DH.util.StoreBank").initAllStore(true);
function onPageLoad() {
	Ext.onReady(function() {
		Ext.tip.QuickTipManager.init();
		Ext.getDom('loading').style.display = 'none'
		var centerPanel = Ext.create("Ext.tab.Panel", {
			layout : 'fit',
			tabWidth : 120,
			hideMode : "offsets",
			deferredRender : false,
			id : "CenterPanel",
			items : [],
			plugins : [ Ext.create('Ext.ux.TabReorderer'), Ext.create('Ext.ux.TabCloseMenu', {
				closeTabText : '关闭当前标签',
				closeOthersTabsText : '关闭其他标签',
				closeAllTabsText : '关闭所有标签'
			}) ],
			listeners : {
				tabchange : function(obj, newCard, oldCard) {
					if (newCard.modelType == 'index') { //刷新线程的启动和关闭
						indexPanel.startTask();
					} else {
						indexPanel.stopTask();
					}
				}
			}
		});
		var indexPanel = Ext.create("DH.index.CenterPanel", {
			title : '首页',
			modelType : 'index',
			indexTabPanel : centerPanel
		})
		centerPanel.add(indexPanel);
		var viewPanel = new Ext.Viewport({
			id : "viewPanel",
			layout : "border",
			items : [ {
				height : 120,
				region : "north",
				items : Ext.create("DH.index.TopPanel")
			}, {
				region : "west",
				collapsible : true,
				title : "导航菜单",
				layout : 'fit',
				items : Ext.create("DH.index.LeftMenu", {
					indexTabPanel : centerPanel
				}),
				width : 160
			}, {
				layout : "fit",
				region : "center",
				items : centerPanel
			} ],
			listeners : {
				afterrender : function() {
					DH.Config.centerHeight = this.height - 157;
					DH.Config.centerWidth = this.width - 160;
					DH.Util.showSuccessToast("页面显示成功！");
					DH.Util.showErrorToast("页面显示成功！");
				}
			}
		});
	});
}
</script>
</html>
