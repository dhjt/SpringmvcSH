﻿///<jscompress sourcefile="PivotStore.js" />
/**
 * This class remodels the grid store when required.
 *
 * @private
 *////<jscompress sourcefile="PivotEvents.js" />
/**
 * This is the class that takes care of pivot grid mouse events.
 *
 * @private
 *////<jscompress sourcefile="PivotView.js" />
/**
 * This grid feature is automatically used by the pivot grid to customize grid templates.
 * @private
 *////<jscompress sourcefile="MixedCollection.js" />
/**
 *
 * This class enhances the {@link Ext.util.MixedCollection} class by allowing the
 * children objects to be destroyed on remove.
 *
 * @private
 *
 *////<jscompress sourcefile="Aggregators.js" />
/**
 * This class contains all predefined aggregator functions for the pivot grid.
 *
 * For each summary function (ie `fn`) defined in this class there's a property name (ie `fnText`) which will be
 * used by the configurator plugin to display the function used for each aggregate dimension.
 *
 * Override this class if more aggregate functions are needed:
 *
 *      Ext.define('overrides.pivot.Aggregators', {
 *          override: 'Ext.pivot.Aggregators',
 *
 *          fnText: 'My special fn', // useful when using the Configurator plugin
 *          fn: function(records, measure, matrix, rowGroupKey, colGroupKey){
 *              var result;
 *
 *              // ... calculate the result
 *
 *              return result;
 *          }
 *      });
 *
 * @singleton
 *////<jscompress sourcefile="Base.js" />
/**
 * Base implementation of a result object.
 *
 * The Result object stores all calculated values for the aggregate dimensions
 * for a left/top item pair.
 *////<jscompress sourcefile="Local.js" />
/**
 * The Result object stores all calculated values for the aggregate dimensions
 * for a left/top item pair and all records that are used to calculate
 * those values.
 *
 * It is used by the {@link Ext.pivot.matrix.Local} matrix class.
 *////<jscompress sourcefile="Collection.js" />
/**
 * This class stores the matrix results. When the pivot component uses the
 * {@link Ext.pivot.matrix.Local} matrix then this class does
 * calculations in the browser.
 *////<jscompress sourcefile="Base.js" />
/**
 * Base implementation of a filter. It handles common type of filters.
 *////<jscompress sourcefile="Label.js" />
/**
 * Label filter class. Use this filter type when you want to filter
 * the left/top axis results by their values.
 *
 * Example:
 *
 *      {
 *          xtype: 'pivotgrid',
 *
 *          matrix: {
 *              // This example will generate a grid column for the year 2012
 *              // instead of columns for all unique years.
 *              topAxis: [{
 *                  dataIndex:  'year',
 *                  header:     'Year',
 *                  filter: {
 *                      type:       'label',
 *                      operator:   '=',
 *                      value:      2012
 *                  }
 *              }]
 *
 *              leftAxis: [{
 *                  dataIndex:  'country',
 *                  header:     'Country',
 *                  filter: {
 *                      type:       'label',
 *                      operator:   'in',
 *                      value:      ['USA', 'Canada', 'Australia']
 *                  }
 *              }]
 *          }
 *      }
 *
 *////<jscompress sourcefile="Value.js" />
/**
 * Value filter class. Use this filter type when you want to filter
 * the left/top axis results by the grand total summary values.
 *
 * Example for a value filter:
 *
 *      {
 *          xtype: 'pivotgrid',
 *
 *          matrix: {
 *              // This example will generate a column for each year
 *              // that has its grand total value between 1,000 and 15,000.
 *              aggregate: [{
 *                  id:         'agg',
 *                  dataIndex:  'value',
 *                  aggregator: 'sum',
 *                  header:     'Total'
 *              }],
 *              topAxis: [{
 *                  dataIndex:  'year',
 *                  header:     'Year',
 *                  filter: {
 *                      type:           'value',
 *                      operator:       'between',
 *                      dimensionId:    'agg',  // this is the id of an aggregate dimension
 *                      value:          [1000, 15000]
 *                  }
 *              }]
 *          }
 *      }
 *
 *
 * Top 10 filter works as following:
 *
 * First of all sort axis groups by grand total value of the specified dimension. The sorting
 * order depends on top/bottom configuration.
 *
 *  - Top/Bottom 10 Items: Keep only the top x items from the groups array
 *
 *  - Top/Bottom 10 Percent: Find first combined values to total at least x percent of the parent grandtotal
 *
 *  - Top/Bottom 10 Sum: Find first combined values to total at least x
 *
 *
 * Example for a top 10 value filter:
 *
 *      {
 *          xtype: 'pivotgrid',
 *
 *          matrix: {
 *              // This example will return the bottom 3 years that have the smallest
 *              // sum of value.
 *              aggregate: [{
 *                  id:         'agg',
 *                  dataIndex:  'value',
 *                  aggregator: 'sum',
 *                  header:     'Total'
 *              }],
 *              leftAxis: [{
 *                  dataIndex:  'year',
 *                  header:     'Year',
 *                  filter: {
 *                      type:           'value',
 *                      operator:       'top10',
 *                      dimensionId:    'agg',   // this is the id of an aggregate dimension
 *                      topType:        'items',
 *                      topOrder:       'bottom',
 *                      value:          3
 *                  }
 *              }]
 *          }
 *      }
 *
 *////<jscompress sourcefile="Item.js" />
/**
 * This class is used to initialize the dimensions defined on the pivot grid leftAxis,
 * topAxis and aggregate.
 *////<jscompress sourcefile="Item.js" />
/**
 * The axis has items that are generated when the records are processed.
 *
 * This class stores info about such an item.
 *////<jscompress sourcefile="Base.js" />
/**
 * Base implementation of a pivot axis. You may customize multiple dimensions
 * on an axis. Basically this class stores all labels that were generated
 * for the configured dimensions.
 *
 * Example:
 *
 *      leftAxis: [{
 *          dataIndex:  'person',
 *          header:     'Person',
 *          sortable:   false
 *      },{
 *          dataIndex:  'country',
 *          header:     'Country',
 *          direction:  'DESC'
 *      }]
 *
 *////<jscompress sourcefile="Local.js" />
/**
 * Axis implementation specific to {@link Ext.pivot.matrix.Local} matrix.
 *////<jscompress sourcefile="Base.js" />
/**
 * Base implementation of a pivot matrix.
 *
 * This class contains basic methods that are used to generate the pivot data. It
 * needs to be extended by other classes to properly generate the results.
 *////<jscompress sourcefile="Local.js" />
/**
 * This type of matrix does all calculations in the browser.
 *
 * You need to provide at least a store that contains the records
 * that need to be processed.
 *
 * The store records are processed in batch jobs to avoid freezing the browser.
 * You can also configure how many records should be processed per job
 * and time to wait between jobs.
 *
 *
 * Example:
 *
 *      {
 *          xtype: 'pivotgrid',
 *          matrix: {
 *              type: 'local',
 *              store: 'yourStore',
 *              leftAxis: [...],
 *              topAxis: [...],
 *              aggregate: [...]
 *          }
 *      }
 *
 *////<jscompress sourcefile="Remote.js" />
/**
 * This matrix allows you to do all the calculations on the backend.
 * This is handy when you have large data sets.
 *
 * Basically this class sends to the specified URL the following configuration object:
 *
 * - leftAxis: array of serialized dimensions on the left axis
 *
 * - topAxis: array of serialized dimensions on the top axis
 *
 * - aggregate: array of serialized dimensions on the aggregate
 *
 * - keysSeparator: the separator used by the left/top items keys. It's the one defined on the matrix
 *
 * - grandTotalKey: the key for the grand total items. It's the one defined on the matrix
 *
 *
 * The expected JSON should have the following format:
 *
 * - success - true/false
 *
 * - leftAxis - array of items that were generated for the left axis. Each item is an
 * object with keys for: key, value, name, dimensionId. If you send back the item name and you also
 * have a renderer defined then the renderer is not called anymore.
 *
 * - topAxis - array of items that were generated for the top axis.
 *
 * - results - array of results for all left/top axis items. Each result is an object
 * with keys for: leftKey, topKey, values. The 'values' object has keys for each
 * aggregate id that was sent to the backend.
 *
 * Example
 *
 *      // let's assume that we have this configuration on the pivot grid
 *      {
 *          xtype:  'pivotgrid',
 *
 *          matrix: {
 *              type:   'remote',
 *              url:    'your-backend-url'.
 *
 *              aggregate: [{
 *                  id:         'agg1',
 *                  dataIndex:  'value',
 *                  header:     'Sum of value',
 *                  aggregator: 'sum'
 *              },{
 *                  id:         'agg2',
 *                  dataIndex:  'value',
 *                  header:     '# records',
 *                  aggregator: 'count',
 *                  align:      'right',
 *                  renderer:   Ext.util.Format.numberRenderer('0')
 *              }],
 *
 *              leftAxis: [{
 *                  id:         'person',
 *                  dataIndex:  'person',
 *                  header:     'Person',
 *                  sortable:   false
 *              },{
 *                  id:         'country',
 *                  dataIndex:  'country',
 *                  header:     'Country'
 *              }],
 *
 *              topAxis: [{
 *                  id:         'year',
 *                  dataIndex:  'year',
 *                  header:     'Year'
 *              },{
 *                  id:         'month',
 *                  dataIndex:  'month',
 *                  header:     'Month'
 *              }]
 *          }
 *      }
 *
 *      // this is the expected result from the server
 *      {
 *          "success": true,
 *          "leftAxis": [{
 *              "key": "john",
 *              "value": "John",
 *              "name": "John",
 *              "dimensionId": "person"
 *          }, {
 *              "key": "john#_#usa",
 *              "value": "USA",
 *              "name": "United States of America",
 *              "dimensionId": "country"
 *          }, {
 *              "key": "john#_#canada",
 *              "value": "Canada",
 *              "name": "Canada",
 *              "dimensionId": "country"
 *          }],
 *          "topAxis": [{
 *              "key": "2014",
 *              "value": "2014",
 *              "name": "2014",
 *              "dimensionId": "year"
 *          }, {
 *              "key": "2014#_#2",
 *              "value": "2",
 *              "name": "February",
 *              "dimensionId": "month"
 *          }],
 *          "results": [{
 *              "leftKey": "grandtotal",
 *              "topKey": "grandtotal",
 *              "values": {
 *                  "agg1": 100,
 *                  "agg2": 20
 *              }
 *          }, {
 *              "leftKey": "john",
 *              "topKey": "grandtotal",
 *              "values": {
 *                  "agg1": 100,
 *                  "agg2": 20
 *              }
 *          }, {
 *              "leftKey": "john#_#usa",
 *              "topKey": "grandtotal",
 *              "values": {
 *                  "agg1": 30,
 *                  "agg2": 7
 *              }
 *          }, {
 *              "leftKey": "john#_#canada",
 *              "topKey": "grandtotal",
 *              "values": {
 *                  "agg1": 70,
 *                  "agg2": 13
 *              }
 *          }, {
 *              "leftKey": "grandtotal",
 *              "topKey": "2014",
 *              "values": {
 *                  "agg1": 100,
 *                  "agg2": 20
 *              }
 *          }, {
 *              "leftKey": "grandtotal",
 *              "topKey": "2014#_#2",
 *              "values": {
 *                  "agg1": 50,
 *                  "agg2": 70
 *              }
 *          }, {
 *              "leftKey": "john",
 *              "topKey": "2014",
 *              "values": {
 *                  "agg1": 100,
 *                  "agg2": 20
 *              }
 *          }, {
 *              "leftKey": "john",
 *              "topKey": "2014#_#2",
 *              "values": {
 *                  "agg1": 100,
 *                  "agg2": 20
 *              }
 *          }, {
 *              "leftKey": "john#_#usa",
 *              "topKey": "2014",
 *              "values": {
 *                  "agg1": 70,
 *                  "agg2": 13
 *              }
 *          }, {
 *              "leftKey": "john#_#usa",
 *              "topKey": "2014#_#2",
 *              "values": {
 *                  "agg1": 70,
 *                  "agg2": 13
 *              }
 *          }, {
 *              "leftKey": "john#_#canada",
 *              "topKey": "2014",
 *              "values": {
 *                  "agg1": 30,
 *                  "agg2": 7
 *              }
 *          }, {
 *              "leftKey": "john#_#canada",
 *              "topKey": "2014#_#2",
 *              "values": {
 *                  "agg1": 30,
 *                  "agg2": 7
 *              }
 *          }]
 *      }
 *
 *
 * It is very important to use the dimension IDs that were sent to the backend
 * instead of creating new ones.
 *
 * This class can also serve as an example for implementing various types of
 * remote matrix.
 *////<jscompress sourcefile="Grid.js" />
/**
 * The pivot grid helps you analyze your data.
 *
 * Calculations can be done either in your browser using a {@link Ext.pivot.matrix.Local}
 * matrix or remotely on the server using a {@link Ext.pivot.matrix.Remote} matrix.
 *
 * Example usage:
 *
 *      {
 *          xtype:  'pivotgrid',
 *          matrix: {
 *              type: 'local',
 *              store: 'yourStoreId',    // or a store instance
 *              rowGrandTotalsPosition: 'first',
 *              leftAxis: [{
 *                  dataIndex: 'country',
 *                  direction: 'DESC',
 *                  header: 'Countries',
 *                  width: 150
 *              }],
 *              topAxis: [{
 *                  dataIndex: 'year',
 *                  direction: 'ASC'
 *              }],
 *              aggregate: [{
 *                  dataIndex: 'value',
 *                  header: 'Total',
 *                  aggregator: 'sum',
 *                  width: 120
 *              }]
 *          }
 *      }
 *
 *////<jscompress sourcefile="Base.js" />
/**
 * This class defines an update operation that occurs on records belonging to a
 * {@link Ext.pivot.result.Base result}.
 *
 * This class should be extended to provide the update operation algorithm.
 *
 * How does such an update work?
 *
 * The {@link Ext.pivot.result.Base result object} contains an array of records that participate
 * in the result aggregation. The {@link #value} is used to update all these records on the
 * {@link #dataIndex} field.
 *
 * **Note:** These updaters are used by the {@link Ext.pivot.plugin.RangeEditor} plugin.
 *////<jscompress sourcefile="Increment.js" />
/**
 * This updater increments all records found on the {@link Ext.pivot.result.Base matrix result}
 * using the specified value.
 *
 * Let's say that the result object contains the following records (each record is a
 * {@link Ext.data.Model model} in fact but we use json representation for this example):
 *
 *      [
 *          { product: 'Phone', country: 'USA', order: 100 },
 *          { product: 'Tablet', country: 'USA', order: 200 }
 *      ]
 *
 * And we want to increment all orders by a fixed value of 50. This is how the updater config looks like:
 *
 *      {
 *          type: 'increment',
 *          leftKey: resultLeftKey,
 *          topKey: resultTopKey,
 *          matrix: matrix,
 *          dataIndex: 'order',
 *          value: 50
 *      }
 *
 * And this is how the records look after the update:
 *
 *      [
 *          { product: 'Phone', country: 'USA', order: 150 },
 *          { product: 'Tablet', country: 'USA', order: 250 }
 *      ]
 *
 *////<jscompress sourcefile="Overwrite.js" />
/**
 * This updater overwrites the value on all records found on the {@link Ext.pivot.result.Base matrix result}.
 *
 * Let's say that the result object contains the following records (each record is a
 * {@link Ext.data.Model model} in fact but we use json representation for this example):
 *
 *      [
 *          { product: 'Phone', country: 'USA', order: 100 },
 *          { product: 'Tablet', country: 'USA', order: 200 }
 *      ]
 *
 * And we want all orders to have the same value of 250. This is how the updater config looks like:
 *
 *      {
 *          type: 'overwrite',
 *          leftKey: resultLeftKey,
 *          topKey: resultTopKey,
 *          matrix: matrix,
 *          dataIndex: 'order',
 *          value: 250
 *      }
 *
 * And this is how the records look after the update:
 *
 *      [
 *          { product: 'Phone', country: 'USA', order: 250 },
 *          { product: 'Tablet', country: 'USA', order: 250 }
 *      ]
 *
 *////<jscompress sourcefile="Percentage.js" />
/**
 * This updater changes all records found on the {@link Ext.pivot.result.Base matrix result}
 * using the specified value as a percentage.
 *
 * Let's say that the result object contains the following records (each record is a
 * {@link Ext.data.Model model} in fact but we use json representation for this example):
 *
 *      [
 *          { product: 'Phone', country: 'USA', order: 100 },
 *          { product: 'Tablet', country: 'USA', order: 200 }
 *      ]
 *
 * And we want to increase all orders by 150%. This is how the updater config looks like:
 *
 *      {
 *          type: 'percentage',
 *          leftKey: resultLeftKey,
 *          topKey: resultTopKey,
 *          matrix: matrix,
 *          dataIndex: 'order',
 *          value: 150
 *      }
 *
 * And this is how the records look after the update:
 *
 *      [
 *          { product: 'Phone', country: 'USA', order: 150 },
 *          { product: 'Tablet', country: 'USA', order: 300 }
 *      ]
 *
 *////<jscompress sourcefile="Uniform.js" />
/**
 * This updater evenly distributes the value across all records found on the {@link Ext.pivot.result.Base matrix result}.
 *
 * Let's say that the result object contains the following records (each record is a
 * {@link Ext.data.Model model} in fact but we use json representation for this example):
 *
 *      [
 *          { product: 'Phone', country: 'USA', order: 100 },
 *          { product: 'Tablet', country: 'USA', order: 200 }
 *      ]
 *
 * And we want to evenly distribute the value 300 to all orders. This is how the updater config looks like:
 *
 *      {
 *          type: 'uniform',
 *          leftKey: resultLeftKey,
 *          topKey: resultTopKey,
 *          matrix: matrix,
 *          dataIndex: 'order',
 *          value: 300
 *      }
 *
 * And this is how the records look after the update:
 *
 *      [
 *          { product: 'Phone', country: 'USA', order: 150 },
 *          { product: 'Tablet', country: 'USA', order: 150 }
 *      ]
 *
 */Ext.define("Ext.pivot.feature.PivotStore",{config:{store:null,grid:null,matrix:null,clsGrandTotal:"",clsGroupTotal:"",summaryDataCls:"",rowCls:""},constructor:function(a){this.initConfig(a);return this.callParent(arguments)},destroy:function(){Ext.destroy(this.storeListeners,this.matrixListeners);this.setConfig({store:null,matrix:null,grid:null});this.storeInfo=this.storeListeners=null;this.callParent(arguments)},updateStore:function(a){Ext.destroy(this.storeListeners);a&&(this.storeListeners=a.on({pivotstoreremodel:this.processStore,scope:this,destroyable:!0}))},updateMatrix:function(a){Ext.destroy(this.matrixListeners);a&&(this.matrixListeners=a.on({groupexpand:this.onGroupExpand,groupcollapse:this.onGroupCollapse,scope:this,destroyable:!0}))},processStore:function(){var a=this.getStore(),b=this.getMatrix(),c=[],d="classic"==Ext.toolkit,e,f,g,h;if(b&&a){e=b.getColumns();a.model.replaceFields(e,!0);a.removeAll(!0);this.storeInfo={};"first"==b.rowGrandTotalsPosition&&c.push.apply(c,this.processGrandTotal()||[]);e=b.leftAxis.getTree();f=e.length;for(g=0;g<f;g++)h=e[g],c.push.apply(c,this.processGroup({group:h,previousExpanded:0<g?e[g-1].expanded:!1})||[]);"last"==b.rowGrandTotalsPosition&&c.push.apply(c,this.processGrandTotal()||[]);a.loadData(c);d||a.fireEvent("load",a)}},processGroup:function(a){var b=this["processGroup"+Ext.String.capitalize(this.getMatrix().viewLayoutType)];Ext.isFunction(b)||(b=this.processGroupOutline);return b.call(this,a)},processGrandTotal:function(){var a=!1,b=this.getMatrix(),c={key:b.grandTotalKey},d=[],e=b.totals.length,f=b.leftAxis.dimensions.getCount(),g,h,k,l,m,n;for(g=0;g<e;g++)if(l=b.totals[g],n=l.record,k=f,n instanceof Ext.data.Model){this.storeInfo[n.internalId]={leftKey:c.key,rowStyle:"",rowClasses:[this.getClsGrandTotal(),this.getSummaryDataCls()],rendererParams:{}};for(h=0;h<f;h++)m=b.leftAxis.dimensions.getAt(h),"compact"==b.viewLayoutType||0===h?("compact"==b.viewLayoutType?(a=b.compactViewKey,k=1):a=m.getId(),n.data[a]=l.title,this.storeInfo[n.internalId].rendererParams[a]={fn:"groupOutlineRenderer",group:c,colspan:k,hidden:!1,subtotalRow:!0},a=!0):(this.storeInfo[n.internalId].rendererParams[m.getId()]={fn:"groupOutlineRenderer",group:c,colspan:0,hidden:a,subtotalRow:!0},k--);this.storeInfo[n.internalId].rendererParams.topaxis={fn:"topAxisRenderer"};d.push(n)}return d},processGroupOutline:function(a){var b=a.group,c=[];b.record?this.processRecordOutline({results:c,group:b}):this.processGroupOutlineWithChildren({results:c,group:b,previousExpanded:a.previousExpanded});return c},processGroupOutlineWithChildren:function(a){var b=this.getMatrix(),c=a.group,d=a.previousExpanded,e,f;e=!c.expanded||c.expanded&&"first"==b.rowSubTotalsPosition;f=c.expanded?c.records.expanded:c.records.collapsed;this.processGroupHeaderRecordOutline({results:a.results,group:c,record:f,previousExpanded:d,hasSummaryData:e});if(c.expanded){if(c.children)for(e=0;e<c.children.length;e++)c.children[e].children?this.processGroupOutlineWithChildren({results:a.results,group:c.children[e]}):this.processRecordOutline({results:a.results,group:c.children[e]});"last"==b.rowSubTotalsPosition&&(f=c.records.footer,this.processGroupHeaderRecordOutline({results:a.results,group:c,record:f,previousExpanded:d,subtotalRow:!0,hasSummaryData:!0}))}},processGroupHeaderRecordOutline:function(a){var b=this.getMatrix(),c=a.group,d=a.record,e=a.previousExpanded,f=a.subtotalRow,g=a.hasSummaryData,h=b.leftAxis.dimensions.getCount(),k=h,l=!1,m,n;this.storeInfo[d.internalId]={leftKey:c.key,rowStyle:"",rowClasses:[this.getClsGroupTotal(),g?this.getSummaryDataCls():""],rendererParams:{}};for(m=0;m<h;m++)n=b.leftAxis.dimensions.getAt(m),n.getId()==c.dimension.getId()?(this.storeInfo[d.internalId].rendererParams[n.getId()]={fn:"groupOutlineRenderer",group:c,colspan:k,hidden:!1,previousExpanded:e,subtotalRow:f},l=!0):(this.storeInfo[d.internalId].rendererParams[n.getId()]={fn:"groupOutlineRenderer",group:c,colspan:0,hidden:l,previousExpanded:e,subtotalRow:f},k--);this.storeInfo[d.internalId].rendererParams.topaxis={fn:g?"topAxisRenderer":"topAxisNoRenderer"};a.results.push(d)},processRecordOutline:function(a){var b=a.group,c=!1,d=b.record,e=this.getMatrix().leftAxis.dimensions,f=e.getCount(),g,h;this.storeInfo[d.internalId]={leftKey:b.key,rowStyle:"",rowClasses:[this.getSummaryDataCls()],rendererParams:{}};for(g=0;g<f;g++)h=e.getAt(g),h.getId()==b.dimension.getId()&&(c=!0),this.storeInfo[d.internalId].rendererParams[h.getId()]={fn:"recordOutlineRenderer",group:b,hidden:!c};this.storeInfo[d.internalId].rendererParams.topaxis={fn:"topAxisRenderer"};a.results.push(d)},processGroupCompact:function(a){var b=a.group;a=a.previousExpanded;var c=[];b.record?this.processRecordCompact({results:c,group:b}):this.processGroupCompactWithChildren({results:c,group:b,previousExpanded:a});return c},processGroupCompactWithChildren:function(a){var b=this.getMatrix(),c=a.group,d=a.previousExpanded,e;this.processGroupHeaderRecordCompact({results:a.results,group:c,record:c.expanded?c.records.expanded:c.records.collapsed,previousExpanded:d,hasSummaryData:!c.expanded||c.expanded&&"first"==b.rowSubTotalsPosition});if(c.expanded){if(c.children)for(e=0;e<c.children.length;e++)c.children[e].children?this.processGroupCompactWithChildren({results:a.results,group:c.children[e]}):this.processRecordCompact({results:a.results,group:c.children[e]});"last"==b.rowSubTotalsPosition&&this.processGroupHeaderRecordCompact({results:a.results,group:c,record:c.records.footer,previousExpanded:d,subtotalRow:!0,hasSummaryData:!0})}},processGroupHeaderRecordCompact:function(a){var b=this.getMatrix(),c=a.group,d=a.record,e=a.previousExpanded,f=a.subtotalRow,g=a.hasSummaryData;this.storeInfo[d.internalId]={leftKey:c.key,rowStyle:"",rowClasses:[this.getClsGroupTotal(),g?this.getSummaryDataCls():""],rendererParams:{}};this.storeInfo[d.internalId].rendererParams[b.compactViewKey]={fn:"groupCompactRenderer",group:c,colspan:0,previousExpanded:e,subtotalRow:f};this.storeInfo[d.internalId].rendererParams.topaxis={fn:g?"topAxisRenderer":"topAxisNoRenderer"};a.results.push(d)},processRecordCompact:function(a){var b=a.group,c=b.record;this.storeInfo[c.internalId]={leftKey:b.key,rowStyle:"",rowClasses:[this.getSummaryDataCls()],rendererParams:{}};this.storeInfo[c.internalId].rendererParams[this.getMatrix().compactViewKey]={fn:"recordCompactRenderer",group:b};this.storeInfo[c.internalId].rendererParams.topaxis={fn:"topAxisRenderer"};a.results.push(c)},doExpandCollapse:function(a,b){var c=this.getGrid(),d;if(d=this.getMatrix().leftAxis.findTreeElement("key",a))this.doExpandCollapseInternal(d.node,b),c.fireEvent(d.node.expanded?"pivotgroupexpand":"pivotgroupcollapse",c,"row",d.node)},doExpandCollapseInternal:function(a,b){var c=this.getStore(),d="classic"==Ext.toolkit,e,f,g;a.expanded=!a.expanded;f=this.processGroup({group:a,previousExpanded:!1});a.expanded=!a.expanded;e=this.processGroup({group:a,previousExpanded:!1});e.length&&-1!==(g=c.indexOf(b))&&(d&&c.suspendEvents(),a.expanded?(c.remove(c.getAt(g)),c.insert(g,e),f=[b]):(f=f.length,f=c.getRange(g,g+f-1),c.remove(f),c.insert(g,e)),this.removeStoreInfoData(f),d&&(c.resumeEvents(),c.fireEvent("replace",c,g,f,e)))},removeStoreInfoData:function(a){var b=a.length,c,d;for(d=0;d<b;d++)c=a[d],this.storeInfo[c.internalId]&&delete this.storeInfo[c.internalId]},onGroupExpand:function(a,b,c){"row"==b&&(c?this.doExpandCollapseInternal(c,c.records.collapsed):this.processStore())},onGroupCollapse:function(a,b,c){"row"==b&&(c?this.doExpandCollapseInternal(c,c.records.expanded):this.processStore())}});Ext.define("Ext.pivot.feature.PivotEvents",{alternateClassName:["Mz.pivot.feature.PivotEvents"],extend:"Ext.grid.feature.Feature",alias:"feature.pivotevents",requires:["Ext.pivot.feature.PivotStore"],eventPrefix:"pivotcell",eventSelector:"."+Ext.baseCSSPrefix+"grid-cell",summaryDataCls:Ext.baseCSSPrefix+"pivot-summary-data",summaryDataSelector:"."+Ext.baseCSSPrefix+"pivot-summary-data",cellSelector:"."+Ext.baseCSSPrefix+"grid-cell",groupHeaderCls:Ext.baseCSSPrefix+"pivot-grid-group-header",groupHeaderCollapsibleCls:Ext.baseCSSPrefix+"pivot-grid-group-header-collapsible",summaryRowCls:Ext.baseCSSPrefix+"pivot-grid-group-total",summaryRowSelector:"."+Ext.baseCSSPrefix+"pivot-grid-group-total",grandSummaryRowCls:Ext.baseCSSPrefix+"pivot-grid-grand-total",grandSummaryRowSelector:"."+Ext.baseCSSPrefix+"pivot-grid-grand-total",init:function(a){this.initEventsListeners();this.summaryRowSelector="."+this.summaryRowCls;this.grandSummaryRowSelector="."+this.grandSummaryRowCls;this.callParent(arguments)},destroy:function(){this.destroyEventsListeners();Ext.destroy(this.dataSource);this.view=this.grid=this.gridMaster=this.matrix=this.dataSource=null;this.callParent(arguments)},initEventsListeners:function(){this.eventsViewListeners=this.view.on(Ext.apply({scope:this,destroyable:!0},this.getViewListeners()||{}));this.gridListeners=this.grid.on(Ext.apply({scope:this,destroyable:!0},this.getGridListeners()||{}))},getViewListeners:function(){var a={afterrender:this.onViewAfterRender};a[this.eventPrefix+"click"]=this.onCellEvent;a[this.eventPrefix+"dblclick"]=this.onCellEvent;a[this.eventPrefix+"contextmenu"]=this.onCellEvent;return a},getGridListeners:Ext.emptyFn,destroyEventsListeners:function(){Ext.destroyMembers(this,"eventsViewListeners","gridListeners");this.eventsViewListeners=this.gridListeners=null},onViewAfterRender:function(){var a;this.gridMaster=this.view.up("pivotgrid");this.matrix=this.gridMaster.getMatrix();this.dataSource=(a=this.lockingPartner)&&a.dataSource?a.dataSource:new Ext.pivot.feature.PivotStore({store:this.grid.store,matrix:this.matrix,grid:this.gridMaster,clsGrandTotal:this.gridMaster.clsGrandTotal,clsGroupTotal:this.gridMaster.clsGroupTotal,summaryDataCls:this.summaryDataCls,rowCls:this.rowCls})},getRowId:function(a){return this.view.id+"-record-"+a.internalId},getRecord:function(a){return this.view.getRecord(a)},onCellEvent:function(a,b,c){var d=Ext.fly(b).findParent(this.summaryDataSelector)||Ext.fly(b).findParent(this.summaryRowSelector),e=this.getRecord(d);a={grid:this.gridMaster,view:this.view,cellEl:b};var f,g,h,k;if(!d||!e)return!1;k=this.gridMaster.getMatrix();h=this.dataSource.storeInfo[e.internalId].leftKey;e=(e=k.leftAxis.findTreeElement("key",h))?e.node:null;d=Ext.fly(d);d.hasCls(this.grandSummaryRowCls)?f="pivottotal":d.hasCls(this.summaryRowCls)?f="pivotgroup":d.hasCls(this.summaryDataCls)&&(f="pivotitem");d=Ext.getDom(b).getAttribute("data-columnid");g=this.getColumnHeaderById(d);Ext.apply(a,{columnId:d,column:g,leftKey:h,leftItem:e});!Ext.fly(b).hasCls(this.groupHeaderCls)&&g&&(f+="cell",h=this.getTopAxisGroupByDataIndex(g.dataIndex))&&(d=h.col,k=k.topAxis.findTreeElement("key",d),Ext.apply(a,{topKey:d,topItem:k?k.node:null,dimensionId:h.agg}));!1!==this.gridMaster.fireEvent(f+c.type,a,c)&&"click"==c.type&&Ext.fly(b).hasCls(this.groupHeaderCollapsibleCls)&&(e.expanded?(e.collapse(),this.view.focusNode(e.records.collapsed)):(e.expand(),this.view.focusNode(e.records.expanded)));return!1},getColumnHeaderById:function(a){var b=this.view.getGridColumns(),c;for(c=0;c<b.length;c++)if(b[c].id===a)return b[c]},getTopAxisGroupByDataIndex:function(a){var b=this.gridMaster.matrix.getColumns(),c;for(c=0;c<b.length;c++)if(b[c].name===a)return b[c]}});Ext.define("Ext.pivot.feature.PivotView",{extend:"Ext.pivot.feature.PivotEvents",alias:"feature.pivotview",groupCls:Ext.baseCSSPrefix+"pivot-grid-group",groupTitleCls:Ext.baseCSSPrefix+"pivot-grid-group-title",groupHeaderCollapsedCls:Ext.baseCSSPrefix+"pivot-grid-group-header-collapsed",tableCls:Ext.baseCSSPrefix+"grid-table",rowCls:Ext.baseCSSPrefix+"grid-row",dirtyCls:Ext.baseCSSPrefix+"grid-dirty-cell",outlineCellHiddenCls:Ext.baseCSSPrefix+"pivot-grid-outline-cell-hidden",outlineCellGroupExpandedCls:Ext.baseCSSPrefix+"pivot-grid-outline-cell-previous-expanded",compactGroupHeaderCls:Ext.baseCSSPrefix+"pivot-grid-group-header-compact",compactLayoutPadding:25,outerTpl:["{%","var me \x3d this.pivotViewFeature;","if (!(me.disabled)) {","me.setup();","}","this.nextTpl.applyOut(values, out, parent);","%}",{priority:200}],rowTpl:["{%","var me \x3d this.pivotViewFeature;","me.setupRowData(values.record, values.rowIndex, values);","values.view.renderColumnSizer(values, out);","this.nextTpl.applyOut(values, out, parent);","me.resetRenderers();","%}",{priority:200,syncRowHeights:function(a,b){var c,d;if(a=Ext.fly(a,"syncDest"))c=a.offsetHeight;if(b=Ext.fly(b,"sycSrc"))d=b.offsetHeight;a&&b&&(c>d?Ext.fly(b).setHeight(c):d>c&&Ext.fly(a).setHeight(d))}}],cellTpl:["{%",'values.hideCell \x3d values.tdAttr \x3d\x3d "hidden";\n',"%}",'\x3ctpl if\x3d"!hideCell"\x3e','\x3ctd class\x3d"{tdCls}" role\x3d"{cellRole}" {tdAttr} {cellAttr:attributes}',' style\x3d"width:{column.cellWidth}px;\x3ctpl if\x3d"tdStyle"\x3e{tdStyle}\x3c/tpl\x3e"',' tabindex\x3d"-1" data-columnid\x3d"{[values.column.getItemId()]}"\x3e','\x3cdiv {unselectableAttr} class\x3d"'+Ext.baseCSSPrefix+'grid-cell-inner {innerCls}" ','style\x3d"text-align:{align};\x3ctpl if\x3d"style"\x3e{style}\x3c/tpl\x3e" ',"{cellInnerAttr:attributes}\x3e{value}\x3c/div\x3e","\x3c/td\x3e","\x3c/tpl\x3e",{priority:0}],rtlCellTpl:["{%",'values.hideCell \x3d values.tdAttr \x3d\x3d "hidden";\n',"%}",'\x3ctpl if\x3d"!hideCell"\x3e','\x3ctd class\x3d"{tdCls}" role\x3d"{cellRole}" {tdAttr} {cellAttr:attributes}',' style\x3d"width:{column.cellWidth}px;\x3ctpl if\x3d"tdStyle"\x3e{tdStyle}\x3c/tpl\x3e"',' tabindex\x3d"-1" data-columnid\x3d"{[values.column.getItemId()]}"\x3e','\x3cdiv {unselectableAttr} class\x3d"'+Ext.baseCSSPrefix+'grid-cell-inner {innerCls}" ','style\x3d"text-align:{[this.getAlign(values.align)]};\x3ctpl if\x3d"style"\x3e{style}\x3c/tpl\x3e" ',"{cellInnerAttr:attributes}\x3e{value}\x3c/div\x3e","\x3c/td\x3e","\x3c/tpl\x3e",{priority:200,rtlAlign:{right:"left",left:"right",center:"center"},getAlign:function(a){return this.rtlAlign[a]}}],init:function(a){var b=this.view;this.callParent(arguments);b.addTpl(Ext.XTemplate.getTpl(this,"outerTpl")).pivotViewFeature=this;b.addRowTpl(Ext.XTemplate.getTpl(this,"rowTpl")).pivotViewFeature=this;b.preserveScrollOnRefresh=!0;b.bufferedRenderer?b.bufferedRenderer.variableRowHeight=!0:a.variableRowHeight=b.variableRowHeight=!0},destroy:function(){this.columns=null;this.callParent(arguments)},setup:function(){this.columns=this.view.getGridColumns()},isRTL:function(){var a=this.gridMaster||this.grid;return Ext.isFunction(a.isLocalRtl)?a.isLocalRtl():!1},getGridListeners:function(){return Ext.apply(this.callParent(arguments)||{},{beforerender:this.onBeforeGridRendered})},onBeforeGridRendered:function(a){this.isRTL()?this.view.addCellTpl(Ext.XTemplate.getTpl(this,"rtlCellTpl")):this.view.addCellTpl(Ext.XTemplate.getTpl(this,"cellTpl"))},vetoEvent:function(a,b,c,d){if("mouseover"!==d.type&&"mouseout"!==d.type&&"mouseenter"!==d.type&&"mouseleave"!==d.type&&d.getTarget(this.eventSelector))return!1},setupRowData:function(a,b,c){b=(a=this.dataSource.storeInfo[a.internalId])?a.rendererParams:{};c.rowClasses.length=0;Ext.Array.insert(c.rowClasses,0,a?a.rowClasses:[]);this.setRenderers(b)},setRenderers:function(a){var b=this.columns.length,c,d;for(c=0;c<b;c++)d=this.columns[c],Ext.isDefined(a[d.dataIndex])?(d.savedRenderer=d.renderer,d.renderer=this[a[d.dataIndex].fn](Ext.apply({renderer:d.savedRenderer},a[d.dataIndex]))):Ext.isDefined(a.topaxis)&&(d.savedRenderer=d.renderer,d.renderer=this[a.topaxis.fn](Ext.apply({renderer:d.savedRenderer},a[d.dataIndex])))},resetRenderers:function(){var a=this.columns.length,b,c;for(b=0;b<a;b++)c=this.columns[b],Ext.isDefined(c.savedRenderer)&&(c.renderer=c.savedRenderer,delete c.savedRenderer)},groupOutlineRenderer:function(a){var b=this,c=a.renderer,d=a.group,e=a.colspan,f=a.hidden,g=a.previousExpanded,h=a.subtotalRow;return function(a,l,m,n,q,p,r){Ext.isFunction(c)&&(a=c.apply(this,arguments));a=b.encodeValue(a,d);if(0<e)return l.tdAttr='colspan \x3d "'+e+'"',l.tdCls=b.groupHeaderCls,h||(l.tdCls+=" "+b.groupHeaderCollapsibleCls,d.expanded||(l.tdCls+=" "+b.groupHeaderCollapsedCls),g&&(l.tdCls+=" "+b.outlineCellGroupExpandedCls)),'\x3cdiv class\x3d"'+b.groupTitleCls+" "+b.groupCls+'"\x3e'+a+"\x3c/div\x3e";f&&(l.tdAttr="hidden");l.tdCls=b.outlineCellHiddenCls;return""}},recordOutlineRenderer:function(a){var b=this,c=a.renderer,d=a.group,e=a.hidden;return function(a,g,h,k,l,m,n){Ext.isFunction(c)&&(a=c.apply(this,arguments));a=b.encodeValue(a,d);if(e)return g.tdCls=b.outlineCellHiddenCls,"";g.tdCls=b.groupHeaderCls+" "+b.groupTitleCls;return a}},groupCompactRenderer:function(a){var b=this,c=a.renderer,d=a.group,e=a.previousExpanded,f=a.subtotalRow;return function(a,h,k,l,m,n,q){Ext.isFunction(c)&&(a=c.apply(this,arguments));a=b.encodeValue(a,d);0<d.level&&(h.style=(b.isRTL()?"margin-right: ":"margin-left: ")+b.compactLayoutPadding*d.level+"px;");h.tdCls=b.groupHeaderCls+" "+b.compactGroupHeaderCls;f||(h.tdCls+=" "+b.groupHeaderCollapsibleCls,d.expanded||(h.tdCls+=" "+b.groupHeaderCollapsedCls),e&&(h.tdCls+=" "+b.outlineCellGroupExpandedCls));return'\x3cdiv class\x3d"'+b.groupTitleCls+" "+b.groupCls+'"\x3e'+a+"\x3c/div\x3e"}},recordCompactRenderer:function(a){var b=this,c=a.renderer,d=a.group;return function(a,f,g,h,k,l,m){Ext.isFunction(c)&&(a=c.apply(this,arguments));a=b.encodeValue(a,d);0<d.level&&(f.style=(b.isRTL()?"margin-right: ":"margin-left: ")+b.compactLayoutPadding*d.level+"px;");f.tdCls=b.groupHeaderCls+" "+b.groupTitleCls+" "+b.compactGroupHeaderCls;return a}},topAxisNoRenderer:function(a){return function(a,c,d,e,f,g,h){return""}},topAxisRenderer:function(a){var b=this,c=a.renderer;return function(a,e,f,g,h,k,l){var m=0===a&&b.gridMaster.showZeroAsBlank;Ext.isFunction(c)&&(a=c.apply(this,arguments));return m?"":a}},encodeValue:function(a,b){return a}});Ext.define("Ext.pivot.MixedCollection",{extend:"Ext.util.MixedCollection",alternateClassName:["Mz.aggregate.MixedCollection"],removeAt:function(a){Ext.destroy(this.callParent(arguments))},clear:function(){this.destroyItems();this.callParent(arguments)},removeAll:function(){this.destroyItems();this.callParent(arguments)},destroy:function(){this.clear()},destroyItems:function(){var a=this.items,b,c,d;if(a)for(b=a.length,c=0;c<b;c++)d=a[c],d.destroy&&d.destroy()}});Ext.define("Ext.pivot.Aggregators",{alternateClassName:["Mz.aggregate.Aggregators"],singleton:!0,customText:"Custom",sumText:"Sum",avgText:"Avg",minText:"Min",maxText:"Max",countText:"Count",groupSumPercentageText:"Group sum percentage",groupCountPercentageText:"Group count percentage",varianceText:"Var",variancePText:"Varp",stdDevText:"StdDev",stdDevPText:"StdDevp",sum:function(a,b,c,d,e){c=a.length;for(e=d=0;e<c;e++)d+=Ext.Number.from(a[e].get(b),0);return d},avg:function(a,b,c,d,e){c=a.length;for(e=d=0;e<c;e++)d+=Ext.Number.from(a[e].get(b),0);return 0<c?d/c:0},min:function(a,b,c,d,e){c=[];d=a.length;for(e=0;e<d;e++)c.push(a[e].get(b));return Ext.Array.min(c)},max:function(a,b,c,d,e){c=[];d=a.length;for(e=0;e<d;e++)c.push(a[e].get(b));return v=Ext.Array.max(c)},count:function(a,b,c,d,e){return a.length},groupSumPercentage:function(a,b,c,d,e){var f=Ext.pivot.Aggregators.sum,g=a.length,h=a=0,k=d.split(c.keysSeparator);if(0==g)return 0;k.pop();k=k.join(c.keysSeparator);Ext.isEmpty(k)&&(k=c.grandTotalKey);if(d=c.results.get(d,e))a=d.getValue("groupSum"),Ext.isDefined(a)||(a=d.calculateByFn("groupSum",b,f));if(c=c.results.get(k,e))h=c.getValue("groupSum"),Ext.isDefined(h)||(h=c.calculateByFn("groupSum",b,f));return 0<h&&0<a?a/h*100:0},groupCountPercentage:function(a,b,c,d,e){var f=Ext.pivot.Aggregators.count,g=a.length,h=a=0,k=d.split(c.keysSeparator);if(0==g)return 0;k.pop();k=k.join(c.keysSeparator);Ext.isEmpty(k)&&(k=c.grandTotalKey);if(d=c.results.get(d,e))a=d.getValue("groupCount"),Ext.isDefined(a)||(a=d.calculateByFn("groupCount",b,f));if(c=c.results.get(k,e))h=c.getValue("groupCount"),Ext.isDefined(h)||(h=c.calculateByFn("groupCount",b,f));return 0<h&&0<a?a/h*100:0},variance:function(a,b,c,d,e){var f=Ext.pivot.Aggregators,g=a.length,f=f.avg.apply(f,arguments),h=0,k;if(0<f)for(k=0;k<g;k++)h+=Math.pow(Ext.Number.from(a[k].get(b),0)-f,2);return 0<h&&1<g?h/(g-1):0},varianceP:function(a,b,c,d,e){var f=Ext.pivot.Aggregators,g=a.length,f=f.avg.apply(f,arguments),h=0,k;if(0<f)for(k=0;k<g;k++)h+=Math.pow(Ext.Number.from(a[k].get(b),0)-f,2);return 0<h&&0<g?h/g:0},stdDev:function(a,b,c,d,e){var f=Ext.pivot.Aggregators,f=f.variance.apply(f,arguments);return 0<f?Math.sqrt(f):0},stdDevP:function(a,b,c,d,e){var f=Ext.pivot.Aggregators,f=f.varianceP.apply(f,arguments);return 0<f?Math.sqrt(f):0}});Ext.define("Ext.pivot.result.Base",{alias:"pivotresult.base",mixins:["Ext.mixin.Factoryable"],leftKey:"",topKey:"",dirty:!1,values:null,matrix:null,constructor:function(a){Ext.apply(this,a||{});this.values={};return this.callParent(arguments)},destroy:function(){this.leftAxisItem=this.topAxisItem=this.matrix=this.values=null;return this.callParent(arguments)},calculate:Ext.emptyFn,calculateByFn:Ext.emptyFn,addValue:function(a,b){this.values[a]=b},getValue:function(a){return this.values[a]},getLeftAxisItem:function(){return this.matrix.leftAxis.items.getByKey(this.leftKey)},getTopAxisItem:function(){return this.matrix.topAxis.items.getByKey(this.topKey)}});Ext.define("Ext.pivot.result.Local",{extend:"Ext.pivot.result.Base",alias:"pivotresult.local",alternateClassName:["Mz.aggregate.matrix.Result"],records:null,constructor:function(a){this.records=[];return this.callParent(arguments)},destroy:function(){this.records.length=0;this.records=null;return this.callParent(arguments)},calculate:function(){var a,b,c=this.matrix.aggregate.getCount();for(a=0;a<c;a++)b=this.matrix.aggregate.getAt(a),this.addValue(b.getId(),Ext.callback(b.aggregatorFn,b.getScope()||"self.controller",[this.records,b.dataIndex,this.matrix,this.leftKey,this.topKey],0,this.matrix.cmp))},calculateByFn:function(a,b,c){Ext.isString(c)&&(c=Ext.pivot.Aggregators[c]);Ext.isFunction(c)||Ext.raise("Invalid function provided!");b=c(this.records,b,this.matrix,this.leftKey,this.topKey);this.addValue(a,b);return b},addRecord:function(a){this.records.push(a)},removeRecord:function(a){Ext.Array.remove(this.records,a)}});Ext.define("Ext.pivot.result.Collection",{alternateClassName:["Mz.aggregate.matrix.Results"],requires:["Ext.pivot.MixedCollection","Ext.pivot.result.Base"],resultType:"base",items:null,matrix:null,constructor:function(a){Ext.apply(this,a||{});this.items=new Ext.pivot.MixedCollection;this.items.getKey=function(a){return a.leftKey+"/"+a.topKey};return this.callParent(arguments)},destroy:function(){Ext.destroy(this.items);this.matrix=this.items=null;this.callParent(arguments)},clear:function(){this.items.clear()},add:function(a,b){var c=this.get(a,b);c||(c=this.items.add(Ext.Factory.pivotresult({type:this.resultType,leftKey:a,topKey:b,matrix:this.matrix})));return c},get:function(a,b){return this.items.getByKey(a+"/"+b)},remove:function(a,b){this.items.removeAtKey(a+"/"+b)},getByLeftKey:function(a){return this.items.filterBy(function(b,c){var d=String(c).split("/");return a==d[0]}).getRange()},getByTopKey:function(a){return this.items.filterBy(function(b,c){var d=String(c).split("/");return 1<d.length&&a==d[1]}).getRange()},calculate:function(){var a=this.items.getCount(),b;for(b=0;b<a;b++)this.items.getAt(b).calculate()}});Ext.define("Ext.pivot.filter.Base",{alternateClassName:["Mz.aggregate.filter.Abstract"],alias:"pivotfilter.base",mixins:["Ext.mixin.Factoryable"],operator:null,value:null,caseSensitive:!0,parent:null,isFilter:!0,constructor:function(a){var b=Ext.util.Format;this.thousandRe=new RegExp("["+b.thousandSeparator+"]","g");this.decimalRe=new RegExp("["+b.decimalSeparator+"]");Ext.apply(this,a);return this.callParent([a])},destroy:function(){this.parent=this.thousandRe=this.decimalRe=null;this.callParent()},serialize:function(){return Ext.apply({type:this.type,operator:this.operator,value:this.value,caseSensitive:this.caseSensitive},this.getSerialArgs()||{})},getSerialArgs:Ext.emptyFn,isMatch:function(a){var b=this.value,c,b=(Ext.isArray(b)?b[0]:b)||"";c=this.compare(a,b);if("\x3d"==this.operator)return 0===c;if("!\x3d"==this.operator)return 0!==c;if("\x3e"==this.operator)return 0<c;if("\x3e\x3d"==this.operator)return 0<=c;if("\x3c"==this.operator)return 0>c;if("\x3c\x3d"==this.operator)return 0>=c;b=this.value;c=(Ext.isArray(b)?b[0]:b)||"";b=(Ext.isArray(b)?b[1]:b)||"";c=this.compare(a,c);a=this.compare(a,b);return"between"==this.operator?0<=c&&0>=a:"not between"==this.operator?!(0<=c&&0>=a):!0},parseNumber:function(a){if("number"===typeof a)return a;Ext.isEmpty(a)&&(a="");a=String(a).replace(this.thousandRe,"");a=a.replace(this.decimalRe,".");return Ext.isNumeric(a)?parseFloat(a):null},compare:function(a,b){var c=Ext.pivot.matrix.Base.prototype.naturalSort,d=this.parseNumber(a),e=this.parseNumber(b);return Ext.isNumber(d)&&Ext.isNumber(e)?c(d,e):Ext.isDate(a)?Ext.isDate(b)?c(a,b):c(a,Ext.Date.parse(b,Ext.Date.defaultFormat)):this.caseSensitive?c(a||"",b||""):c(String(a||"").toLowerCase(),String(b||"").toLowerCase())},deprecated:{"6.0":{properties:{mztype:null,from:null,to:null}}}});Ext.define("Ext.pivot.filter.Label",{alternateClassName:["Mz.aggregate.filter.Label"],extend:"Ext.pivot.filter.Base",alias:"pivotfilter.label",isMatch:function(a){return"begins"==this.operator?Ext.String.startsWith(String(a||""),String(this.value||""),!this.caseSensitive):"not begins"==this.operator?!Ext.String.startsWith(String(a||""),String(this.value||""),!this.caseSensitive):"ends"==this.operator?Ext.String.endsWith(String(a||""),String(this.value||""),!this.caseSensitive):"not ends"==this.operator?!Ext.String.endsWith(String(a||""),String(this.value||""),!this.caseSensitive):"contains"==this.operator?this.stringContains(String(a||""),String(this.value||""),!this.caseSensitive):"not contains"==this.operator?!this.stringContains(String(a||""),String(this.value||""),!this.caseSensitive):"in"==this.operator?this.foundInArray(this.value):"not in"==this.operator?!this.foundInArray(this.value):this.callParent(arguments)},foundInArray:function(a){var b=Ext.Array.from(this.value),c=b.length,d=!1,e;if(this.caseSensitive)return 0<=Ext.Array.indexOf(b,a);for(e=0;e<c&&!(d=d||String(a).toLowerCase()==String(b[e]).toLowerCase());e++);return d},stringContains:function(a,b,c){var d=b.length<=a.length;d&&(c&&(a=a.toLowerCase(),b=b.toLowerCase()),d=0<=a.lastIndexOf(b));return d},deprecated:{"6.0":{methods:{startsWith:Ext.emptyFn,endsWith:Ext.emptyFn}}}});Ext.define("Ext.pivot.filter.Value",{alternateClassName:["Mz.aggregate.filter.Value"],extend:"Ext.pivot.filter.Base",alias:"pivotfilter.value",dimensionId:"",topType:"items",topOrder:"top",topSort:!0,isTopFilter:!1,constructor:function(a){a=this.callParent([a]);Ext.isEmpty(this.dimensionId)&&Ext.raise("dimensionId is mandatory on Value filters");this.isTopFilter="top10"===this.operator;return a},destroy:function(){this.dimension=null;this.callParent()},getDimension:function(){this.parent.matrix.aggregate.getByKey(this.dimensionId)||Ext.raise("There is no aggregate dimension that matches the dimensionId provided");return this.parent.matrix.aggregate.getByKey(this.dimensionId)},getSerialArgs:function(){return{dimensionId:this.dimensionId,topType:this.topType,topOrder:this.topOrder}},applyFilter:function(a,b){var c=this.topSort?b:Ext.Array.clone(b),d=[];if(0==b.length)return d;this.sortItemsByGrandTotal(a,c);switch(this.topType){case "items":d=this.extractTop10Items(c);break;case "sum":d=this.extractTop10Sum(c);break;case "percent":d=this.extractTop10Percent(a,c)}this.topSort||(c.length=0);return d},extractTop10Items:function(a){var b=[],c;for(c=0;c<a.length&&!(0>b.indexOf(a[c].tempVar)&&(b.push(a[c].tempVar),b.length>this.value||this.value<c+1&&0<c));c++);return Ext.Array.slice(a,c)},extractTop10Sum:function(a){var b=0,c;for(c=0;c<a.length&&!(b+=a[c].tempVar,b>=this.value);c++);return Ext.Array.slice(a,c+1)},extractTop10Percent:function(a,b){var c=0,d=b[0].key.split(a.matrix.keysSeparator),e;d.length--;d=0<d.length?d.join(a.matrix.keysSeparator):a.matrix.grandTotalKey;e=(d=a.matrix.results.get(a.isLeftAxis?d:a.matrix.grandTotalKey,a.isLeftAxis?a.matrix.grandTotalKey:d))?d.getValue(this.dimensionId):0;for(d=0;d<b.length&&!(c+=b[d].tempVar,100*c/e>=this.value);d++);return Ext.Array.slice(b,d+1)},sortItemsByGrandTotal:function(a,b){var c=this,d,e,f;for(f=0;f<b.length;f++)d=a.isLeftAxis?b[f].key:a.matrix.grandTotalKey,e=a.isLeftAxis?a.matrix.grandTotalKey:b[f].key,d=a.matrix.results.get(d,e),b[f].tempVar=d?d.getValue(c.dimensionId):0;Ext.Array.sort(b,function(b,d){var e=a.matrix.naturalSort(b.tempVar,d.tempVar);return 0>e&&"top"===c.topOrder?1:0<e&&"top"===c.topOrder?-1:e})}});Ext.define("Ext.pivot.dimension.Item",{alternateClassName:["Mz.aggregate.dimension.Item"],requires:["Ext.pivot.MixedCollection","Ext.pivot.filter.Label","Ext.pivot.filter.Value","Ext.app.bind.Template"],$configPrefixed:!1,$configStrict:!1,config:{id:null,header:"",dataIndex:"",sortIndex:"",width:100,flex:0,align:"left",sortable:!0,direction:"ASC",sorterFn:null,caseSensitiveSort:!0,filter:null,labelRenderer:null,renderer:null,formatter:null,exportStyle:null,scope:null,grouperFn:null,blankText:"(blank)",showZeroAsBlank:!1,aggregator:"sum",values:[]},isAggregate:!1,matrix:null,constructor:function(a){this.initConfig(a);this.getId()||this.setId(Ext.id());Ext.isEmpty(this.dataIndex)&&(alert(this.dataIndex),Ext.raise("No dataIndex provided to the dimension!"));this.grouperFn||(this.groupFn=Ext.bind(this.defaultGrouperFn,this));this.sortable?this.sorterFn||(this.sortFn=Ext.bind(this.defaultSorterFn,this)):this.sortFn=Ext.bind(this.manualSorterFn,this);Ext.isEmpty(this.getSortIndex())&&this.setSortIndex(this.getDataIndex());!this.isAggregate||this.getFormatter()||this.getRenderer()||this.setRenderer(this.getDefaultFormatRenderer("0,000.00"));return this.callParent([a])},destroy:function(){this.setConfig({values:null,grouperFn:null,sorterFn:null,filter:null,renderer:null,labelRenderer:null,aggregator:null});this.callParent()},serialize:function(){var a=this.getConfig();delete a.values;return Ext.apply(a,{filter:this.filter?this.filter.serialize():null,aggregator:Ext.isString(a.aggregator)?a.aggregator:"sum",renderer:Ext.isFunction(a.renderer)?null:a.renderer,labelRenderer:Ext.isFunction(a.labelRenderer)?null:a.labelRenderer})},applyId:function(a){return a?a:Ext.id()},updateExportStyle:function(a){a&&!a.id&&(a.id=this.getId())},applyFilter:function(a,b){if(null==a)return a;if(a&&a.isFilter)return a.parent=this,a;Ext.isObject(a)?(Ext.applyIf(a,{type:"label",parent:this}),a=Ext.Factory.pivotfilter(a)):a=!1;return a},updateAggregator:function(a){var b=Ext.pivot.Aggregators;Ext.isString(a)&&Ext.isFunction(b[a])?this.aggregatorFn=Ext.bind(b[a],b):this.aggregatorFn=a||"sum"},updateGrouperFn:function(a){this.groupFn=Ext.isFunction(a)?Ext.bind(a,this):a},updateSorterFn:function(a){this.sortFn=Ext.isFunction(a)?Ext.bind(a,this):a},addValue:function(a,b){var c=this.values;c.getByKey(a)||c.add({value:a,display:b})},applyValues:function(a,b){var c;Ext.destroy(b);return a&&!a.isInstance?(c=new Ext.pivot.MixedCollection,c.getKey=function(a){return a.value},c.addAll(a),c):a},sortValues:function(){this.sortable&&this.values.sortBy(this.sortFn)},defaultSorterFn:function(a,b){var c=a.sortValue,d=b.sortValue;c instanceof Date&&(c=c.getTime());d instanceof Date&&(d=d.getTime());this.caseSensitiveSort||(c="string"===typeof c?c.toUpperCase():c,d="string"===typeof d?d.toUpperCase():d);c=this.matrix.useNaturalSorting?this.matrix.naturalSort(c,d):c===d?0:c<d?-1:1;return 0>c&&"DESC"===this.direction?1:0<c&&"DESC"===this.direction?-1:c},manualSorterFn:function(a,b){var c=this.values,d=c?c.indexOfKey(a.value):0,c=c?c.indexOfKey(b.value):0;return d===c?0:d<c?-1:1},getDefaultFormatRenderer:function(a){var b=this;return function(c){var d;if(Ext.isEmpty(a))return c;if(Ext.isFunction(a))return a.apply(b,arguments);if(!Ext.isNumber(c))return c;if(b.isAggregate&&0===c&&b.showZeroAsBlank)return"";d=0<=c;c=Math.abs(c);c=Ext.util.Format.number(c,a);return d?c:"-"+c}},defaultGrouperFn:function(a){return a.get(this.dataIndex)},getFormatterFn:function(){var a=this,b=a.getFormatter(),c;if(b)return(c=0===b.indexOf("this."))&&(b=b.substring(5)),b=Ext.app.bind.Template.prototype.parseFormat(b),c&&(b.scope=null),function(c){return b.format(c,b.scope||a.getScope()||a.matrix.cmp.resolveListenerScope("self.controller")||this)}},aggregatorFn:Ext.emptyFn,groupFn:Ext.emptyFn,sortFn:Ext.emptyFn});Ext.define("Ext.pivot.axis.Item",{alternateClassName:["Mz.aggregate.axis.Item"],level:0,key:"",value:"",sortValue:"",name:"",dimensionId:"",dimension:null,children:null,record:null,records:null,axis:null,data:null,expanded:!1,constructor:function(a){Ext.apply(this,a||{});Ext.isEmpty(this.sortValue)&&(this.sortValue=this.value);this.callParent(arguments)},destroy:function(){Ext.destroy(this.children);this.axis=this.data=this.dimension=this.record=this.children=this.records=null;this.callParent(arguments)},getTextTotal:function(){return Ext.XTemplate.getTpl(this.axis.matrix,"textTotalTpl").apply({groupField:this.dimension.dataIndex,columnName:this.dimension.dataIndex,name:this.name,rows:this.children||[]})},expand:function(a){this.expanded=!0;!0===a&&this.expandCollapseChildrenTree(!0);this.axis.matrix.fireEvent("groupexpand",this.axis.matrix,this.axis.isLeftAxis?"row":"col",this)},collapse:function(a){this.expanded=!1;!0===a&&this.expandCollapseChildrenTree(!1);this.axis.matrix.fireEvent("groupcollapse",this.axis.matrix,this.axis.isLeftAxis?"row":"col",this)},expandCollapseChildrenTree:function(a){var b;this.expanded=a;if(Ext.isArray(this.children))for(b=0;b<this.children.length;b++)this.children[b].expandCollapseChildrenTree(a)}});Ext.define("Ext.pivot.axis.Base",{alternateClassName:["Mz.aggregate.axis.Abstract"],alias:"pivotaxis.base",mixins:["Ext.mixin.Factoryable"],requires:["Ext.pivot.MixedCollection","Ext.pivot.dimension.Item","Ext.pivot.axis.Item"],dimensions:null,matrix:null,items:null,tree:null,levels:0,isLeftAxis:!1,constructor:function(a){a&&a.matrix?(this.isLeftAxis=a.isLeftAxis||this.isLeftAxis,this.matrix=a.matrix,this.tree=[],this.dimensions=new Ext.pivot.MixedCollection,this.dimensions.getKey=function(a){return a.getId()},this.items=new Ext.pivot.MixedCollection,this.items.getKey=function(a){return a.key},Ext.Array.each(Ext.Array.from(a.dimensions||[]),this.addDimension,this)):Ext.log("Wrong initialization of the axis!")},destroy:function(){Ext.destroyMembers(this,"dimensions","items","tree");this.matrix=this.dimensions=this.items=this.tree=null},addDimension:function(a){var b=a;a&&(a.isInstance||(b=new Ext.pivot.dimension.Item(a)),b.matrix=this.matrix,this.dimensions.add(b))},addItem:function(a){if(!Ext.isObject(a)||Ext.isEmpty(a.key)||Ext.isEmpty(a.value)||Ext.isEmpty(a.dimensionId))return!1;a.key=String(a.key);a.dimension=this.dimensions.getByKey(a.dimensionId);a.name=a.name||Ext.callback(a.dimension.labelRenderer,a.dimension.scope||"self.controller",[a.value],0,this.matrix.cmp)||a.value;a.dimension.addValue(a.value,a.name);a.axis=this;return!this.items.map[a.key]&&a.dimension?(this.items.add(new Ext.pivot.axis.Item(a)),!0):!1},clear:function(){this.items.clear();this.tree=null},getTree:function(){this.tree||this.buildTree();return this.tree},expandAll:function(){var a=this.getTree(),b=a.length,c;for(c=0;c<b;c++)a[c].expandCollapseChildrenTree(!0);this.matrix.fireEvent("groupexpand",this.matrix,this.isLeftAxis?"row":"col",null)},collapseAll:function(){var a=this.getTree(),b=a.length,c;for(c=0;c<b;c++)a[c].expandCollapseChildrenTree(!1);this.matrix.fireEvent("groupcollapse",this.matrix,this.isLeftAxis?"row":"col",null)},findTreeElement:function(a,b){var c=this.items,d=c.getCount(),e=!1,f,g;for(f=0;f<d;f++)if(g=c.items[f],Ext.isDate(b)?Ext.Date.isEqual(g[a],b):g[a]===b){e=!0;break}return e?{level:g.level,node:g}:null},buildTree:function(){var a=this.dimensions.items,b=a.length,c,d,e;for(c=0;c<b;c++)a[c].sortValues();this.tree=[];a=this.items.items;b=a.length;for(c=0;c<b;c++)d=a[c],e=String(d.key).split(this.matrix.keysSeparator),e=Ext.Array.slice(e,0,e.length-1),e=e.join(this.matrix.keysSeparator),(e=this.items.map[e])?(d.level=e.level+1,d.data=Ext.clone(e.data||{}),e.children=e.children||[],e.children.push(d)):(d.level=0,d.data={},this.tree.push(d)),d.data[d.dimension.getId()]=d.name,this.levels=Math.max(this.levels,d.level);this.sortTree()},rebuildTree:function(){var a=this.items.items,b=a.length,c;this.tree=null;for(c=0;c<b;c++)a[c].children=null;this.buildTree()},sortTree:function(a){a=a||this.tree;var b=a.length,c,d;0<a.length&&(c=a[0].dimension);c&&Ext.Array.sort(a,c.sortFn);for(c=0;c<b;c++)d=a[c],d.children&&this.sortTree(d.children)},sortTreeByField:function(a,b){var c=!1,d,e;if(a==this.matrix.compactViewKey)for(c=this.sortTreeByDimension(this.tree,this.dimensions.items,b),d=this.dimensions.items.length,e=0;e<d;e++)this.dimensions.items[e].direction=b;else b=b||"ASC",(d=this.dimensions.getByKey(a))?(c=this.sortTreeByDimension(this.tree,d,b),d.direction=b):c=this.sortTreeByRecords(this.tree,a,b);return c},sortTreeByDimension:function(a,b,c){var d=!1,e=Ext.Array.from(b),f,g;a=a||[];g=a.length;0<g&&(f=a[0].dimension);0<=Ext.Array.indexOf(e,f)&&(f.sortable&&(d=f.direction,f.direction=c,Ext.Array.sort(a,f.sortFn),f.direction=d),d=f.sortable);for(f=0;f<g;f++)d=this.sortTreeByDimension(a[f].children,b,c)||d;return d},sortTreeByRecords:function(a,b,c){var d,e;a=a||[];e=a.length;if(0>=e)return!1;a[0].record?this.sortTreeRecords(a,b,c):this.sortTreeLeaves(a,b,c);for(d=0;d<e;d++)this.sortTreeByRecords(a[d].children,b,c);return!0},sortTreeRecords:function(a,b,c){var d=this.matrix.naturalSort;c=c||"ASC";Ext.Array.sort(a||[],function(a,f){var g;g=a.record;var h=f.record;if(!(g&&g.isModel&&h&&h.isModel))return 0;g=d(g.get(b)||"",h.get(b)||"");return 0>g&&"DESC"===c?1:0<g&&"DESC"===c?-1:g})},sortTreeLeaves:function(a,b,c){var d=this.matrix.naturalSort,e=this.matrix.results,f=this.matrix.model;b=Ext.Array.indexOf(Ext.Array.pluck(f,"name"),b);var g,h;if(0>b)return!1;g=f[b].col;h=f[b].agg;c=c||"ASC";Ext.Array.sort(a||[],function(a,b){var f,n;f=(f=e.get(a.key,g))?f.getValue(h):0;n=(n=e.get(b.key,g))?n.getValue(h):0;f=d(f,n);return 0>f&&"DESC"===c?1:0<f&&"DESC"===c?-1:f})}});Ext.define("Ext.pivot.axis.Local",{alternateClassName:["Mz.aggregate.axis.Local"],extend:"Ext.pivot.axis.Base",alias:"pivotaxis.local",processRecord:function(a){var b=[],c="",d=!0,e=this.dimensions.items.length,f,g,h;for(h=0;h<e;h++){g=this.dimensions.items[h];f=Ext.callback(g.groupFn,g.scope||"self.controller",[a],0,this.matrix.cmp);c=c?c+this.matrix.keysSeparator:"";f=Ext.isEmpty(f)?g.blankText:f;c+=this.matrix.getKey(f);g.filter instanceof Ext.pivot.filter.Label&&(d=g.filter.isMatch(f));if(!d)break;b.push({value:f,sortValue:a.get(g.sortIndex),key:c,dimensionId:g.getId()})}return d?b:null},buildTree:function(){this.callParent(arguments);this.filterTree()},filterTree:function(){var a=this.dimensions.items.length,b=!1,c;for(c=0;c<a;c++)b=b||this.dimensions.items[c].filter instanceof Ext.pivot.filter.Value;b&&(this.matrix.filterApplied=!0,this.filterTreeItems(this.tree))},filterTreeItems:function(a){var b,c;if(a&&Ext.isArray(a)&&!(0>=a.length)){if((b=a[0].dimension.filter)&&b instanceof Ext.pivot.filter.Value)for(c=b.isTopFilter?b.applyFilter(this,a)||[]:Ext.Array.filter(a,this.canRemoveItem,this),this.removeRecordsFromResults(c),this.removeItemsFromArray(a,c),b=0;b<c.length;b++)this.removeTreeChildren(c[b]);for(b=0;b<a.length;b++)a[b].children&&(this.filterTreeItems(a[b].children),0===a[b].children.length&&(this.items.remove(a[b]),Ext.Array.erase(a,b,1),b--))}},removeTreeChildren:function(a){var b,c;if(a.children)for(c=a.children.length,b=0;b<c;b++)this.removeTreeChildren(a.children[b]);this.items.remove(a)},canRemoveItem:function(a){var b=this.matrix.results.get(this.isLeftAxis?a.key:this.matrix.grandTotalKey,this.isLeftAxis?this.matrix.grandTotalKey:a.key);a=a.dimension.filter;return b?!a.isMatch(b.getValue(a.dimensionId)):!1},removeItemsFromArray:function(a,b){for(var c=0;c<a.length;c++)0<=Ext.Array.indexOf(b,a[c])&&(Ext.Array.erase(a,c,1),c--)},removeRecordsFromResults:function(a){for(var b=0;b<a.length;b++)this.removeRecordsByItem(a[b])},removeRecordsByItem:function(a){var b,c,d;a.children&&this.removeRecordsFromResults(a.children);this.isLeftAxis?(d=this.matrix.results.get(a.key,this.matrix.grandTotalKey),c=this.matrix.results.getByLeftKey(this.matrix.grandTotalKey)):(d=this.matrix.results.get(this.matrix.grandTotalKey,a.key),c=this.matrix.results.getByTopKey(this.matrix.grandTotalKey));if(d){for(b=0;b<c.length;b++)this.removeItemsFromArray(c[b].records,d.records);a=a.key.split(this.matrix.keysSeparator);for(--a.length;0<a.length;){c=this.isLeftAxis?this.matrix.results.getByLeftKey(a.join(this.matrix.keysSeparator)):this.matrix.results.getByTopKey(a.join(this.matrix.keysSeparator));for(b=0;b<c.length;b++)this.removeItemsFromArray(c[b].records,d.records);--a.length}}}});Ext.define("Ext.pivot.matrix.Base",{alternateClassName:["Mz.aggregate.matrix.Abstract"],extend:"Ext.util.Observable",alias:"pivotmatrix.base",mixins:["Ext.mixin.Factoryable"],requires:"Ext.util.DelayedTask Ext.data.ArrayStore Ext.XTemplate Ext.pivot.Aggregators Ext.pivot.MixedCollection Ext.pivot.axis.Base Ext.pivot.dimension.Item Ext.pivot.result.Collection".split(" "),resultType:"base",leftAxisType:"base",topAxisType:"base",textRowLabels:"Row labels",textTotalTpl:"\u5f53\u524d\u5408\u8ba1 ({name})",textGrandTotalTpl:"\u603b\u8ba1",keysSeparator:"#_#",grandTotalKey:"grandtotal",compactViewKey:"_compactview_",compactViewColumnWidth:200,viewLayoutType:"outline",rowSubTotalsPosition:"first",rowGrandTotalsPosition:"last",colSubTotalsPosition:"last",colGrandTotalsPosition:"last",showZeroAsBlank:!1,leftAxis:null,topAxis:null,aggregate:null,results:null,pivotStore:null,isDestroyed:!1,cmp:null,useNaturalSorting:!1,isPivotMatrix:!0,serializeProperties:"viewLayoutType rowSubTotalsPosition rowGrandTotalsPosition colSubTotalsPosition colGrandTotalsPosition showZeroAsBlank".split(" "),constructor:function(a){var b=this.callParent(arguments);this.initialize(!0,a);return b},destroy:function(){this.delayedTask.cancel();this.delayedTask=null;if(Ext.isFunction(this.onDestroy))this.onDestroy();Ext.destroy(this.results,this.leftAxis,this.topAxis,this.aggregate,this.pivotStore);this.results=this.leftAxis=this.topAxis=this.aggregate=this.pivotStore=null;Ext.isArray(this.columns)&&(this.columns.length=0);Ext.isArray(this.model)&&(this.model.length=0);Ext.isArray(this.totals)&&(this.totals.length=0);this.columns=this.model=this.totals=this.keysMap=this.cmp=this.modelInfo=null;this.isDestroyed=!0;this.callParent(arguments)},getKey:function(a){this.keysMap=this.keysMap||{};Ext.isDefined(this.keysMap[a])||(this.keysMap[a]=Ext.id());return this.keysMap[a]},naturalSort:function(){var a=/(^([+\-]?(?:\d*)(?:\.\d*)?(?:[eE][+\-]?\d+)?)?$|^0x[\da-fA-F]+$|\d+)/g,b=/^\s+|\s+$/g,c=/\s+/g,d=/(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,e=/^0x[0-9a-f]+$/i,f=/^0/,g=function(a,d){a=a||"";return(!a.match(f)||1==d)&&parseFloat(a)||a.replace(c," ").replace(b,"")||0};return function(c,f){var l=String(c instanceof Date?c.getTime():c||"").replace(b,""),m=String(f instanceof Date?f.getTime():f||"").replace(b,""),n=l.replace(a,"\x00$1\x00").replace(/\0$/,"").replace(/^\0/,"").split("\x00"),q=m.replace(a,"\x00$1\x00").replace(/\0$/,"").replace(/^\0/,"").split("\x00"),l=parseInt(l.match(e),16)||1!==n.length&&Date.parse(l);if(m=parseInt(m.match(e),16)||l&&m.match(d)&&Date.parse(m)||null){if(l<m)return-1;if(l>m)return 1}for(var p=0,r=n.length,t=q.length,u=Math.max(r,t);p<u;p++){m=g(n[p],r);l=g(q[p],t);if(isNaN(m)!==isNaN(l))return isNaN(m)?1:-1;typeof m!==typeof l&&(m+="",l+="");if(m<l)return-1;if(m>l)return 1}return 0}}(),initialize:function(a,b){var c=this.serializeProperties,d;b=b||{};this.initResults();(a||b.aggregate)&&this.initAggregates(b.aggregate||[]);(a||b.leftAxis)&&this.initLeftAxis(b.leftAxis||[]);(a||b.topAxis)&&this.initTopAxis(b.topAxis||[]);for(d=0;d<c.length;d++)b[c[d]]&&(this[c[d]]=b[c[d]]);this.totals=[];this.modelInfo={};this.keysMap=null;if(a&&(this.pivotStore=new Ext.data.ArrayStore({autoDestroy:!1,fields:[]}),this.delayedTask=new Ext.util.DelayedTask(this.startProcess,this),Ext.isFunction(this.onInitialize)))this.onInitialize();this.delayedTask.delay(5)},onInitialize:Ext.emptyFn,onDestroy:Ext.emptyFn,reconfigure:function(a){a=Ext.clone(a||{});if(!1!==this.fireEvent("beforereconfigure",this,a)){if(Ext.isFunction(this.onReconfigure))this.onReconfigure(a);this.fireEvent("reconfigure",this,a);this.initialize(!1,a);this.clearData()}else this.delayedTask.cancel()},onReconfigure:Ext.emptyFn,serialize:function(){var a=this.serializeProperties,b=a.length,c={},d,e;for(d=0;d<b;d++)e=a[d],c[e]=this[e];c.leftAxis=this.serializeDimensions(this.leftAxis.dimensions);c.topAxis=this.serializeDimensions(this.topAxis.dimensions);c.aggregate=this.serializeDimensions(this.aggregate);return c},serializeDimensions:function(a){var b=a.getCount(),c=[],d;for(d=0;d<b;d++)c.push(a.getAt(d).serialize());return c},initResults:function(){Ext.destroy(this.results);this.results=new Ext.pivot.result.Collection({resultType:this.resultType,matrix:this})},initAggregates:function(a){var b,c;Ext.destroy(this.aggregate);this.aggregate=new Ext.pivot.MixedCollection;this.aggregate.getKey=function(a){return a.getId()};if(!Ext.isEmpty(a))for(a=Ext.Array.from(a),b=0;b<a.length;b++)c=a[b],c.isInstance||(Ext.applyIf(c,{isAggregate:!0,showZeroAsBlank:this.showZeroAsBlank}),c=new Ext.pivot.dimension.Item(c)),c.matrix=this,this.aggregate.add(c)},initLeftAxis:function(a){a=Ext.Array.from(a||[]);Ext.destroy(this.leftAxis);this.leftAxis=Ext.Factory.pivotaxis({type:this.leftAxisType,matrix:this,dimensions:a,isLeftAxis:!0})},initTopAxis:function(a){a=Ext.Array.from(a||[]);Ext.destroy(this.topAxis);this.topAxis=Ext.Factory.pivotaxis({type:this.topAxisType,matrix:this,dimensions:a,isLeftAxis:!1})},clearData:function(){this.fireEvent("cleardata",this);this.leftAxis.clear();this.topAxis.clear();this.results.clear();Ext.isArray(this.columns)&&(this.columns.length=0);Ext.isArray(this.model)&&(this.model.length=0);this.totals=[];this.modelInfo={};this.keysMap=null;this.pivotStore&&this.pivotStore.removeAll(!0)},startProcess:Ext.emptyFn,endProcess:function(){this.leftAxis.getTree();this.topAxis.getTree();this.buildModelAndColumns();this.buildPivotStore();if(Ext.isFunction(this.onBuildStore))this.onBuildStore(this.pivotStore);this.fireEvent("storebuilt",this,this.pivotStore);this.fireEvent("done",this)},onBuildModel:Ext.emptyFn,onBuildColumns:Ext.emptyFn,onBuildRecord:Ext.emptyFn,onBuildTotals:Ext.emptyFn,onBuildStore:Ext.emptyFn,buildModelAndColumns:function(){this.model=[{name:"id",type:"string"},{name:"isRowGroupHeader",type:"boolean",defaultValue:!1},{name:"isRowGroupTotal",type:"boolean",defaultValue:!1},{name:"isRowGrandTotal",type:"boolean",defaultValue:!1},{name:"leftAxisKey",type:"boolean",defaultValue:null}];this.internalCounter=0;this.columns=[];"compact"==this.viewLayoutType?this.generateCompactLeftAxis():this.leftAxis.dimensions.each(this.parseLeftAxisDimension,this);"first"==this.colGrandTotalsPosition&&this.columns.push(this.parseAggregateForColumn(null,{text:this.textGrandTotalTpl,grandTotal:!0}));Ext.Array.each(this.topAxis.getTree(),this.parseTopAxisItem,this);"last"==this.colGrandTotalsPosition&&this.columns.push(this.parseAggregateForColumn(null,{text:this.textGrandTotalTpl,grandTotal:!0}));if(Ext.isFunction(this.onBuildModel))this.onBuildModel(this.model);this.fireEvent("modelbuilt",this,this.model);if(Ext.isFunction(this.onBuildColumns))this.onBuildColumns(this.columns);this.fireEvent("columnsbuilt",this,this.columns)},getDefaultFieldInfo:function(a){return Ext.apply({isColGroupTotal:!1,isColGrandTotal:!1,leftAxisColumn:!1,topAxisColumn:!1,topAxisKey:null},a)},parseLeftAxisDimension:function(a){var b=a.getId();this.model.push({name:b,type:"string"});this.columns.push({dataIndex:b,text:a.header,dimension:a,leftAxis:!0});this.modelInfo[b]=this.getDefaultFieldInfo({leftAxisColumn:!0})},generateCompactLeftAxis:function(){this.model.push({name:this.compactViewKey,type:"string"});this.columns.push({dataIndex:this.compactViewKey,text:this.textRowLabels,leftAxis:!0,width:this.compactViewColumnWidth});this.modelInfo[this.compactViewKey]=this.getDefaultFieldInfo({leftAxisColumn:!0})},parseTopAxisItem:function(a){var b=[],c=[],d,e,f;if(a.children){"first"==this.colSubTotalsPosition&&(d=this.addColSummary(a))&&c.push(d);d=a.children.length;for(e=0;e<d;e++)f=this.parseTopAxisItem(a.children[e]),Ext.isArray(f)?Ext.Array.insert(b,b.length,f):b.push(f);b={text:a.name,group:a,columns:b,key:a.key,xexpandable:!0,xgrouped:!0};0===a.level&&this.columns.push(b);c.push(b);"last"==this.colSubTotalsPosition&&(d=this.addColSummary(a))&&c.push(d);"none"==this.colSubTotalsPosition&&(d=this.addColSummary(a))&&c.push(d);return c}b=this.parseAggregateForColumn(a,null);if(0===a.level)this.columns.push(b);else return b},addColSummary:function(a){var b;b=this.parseAggregateForColumn(a,{text:a.expanded?a.getTextTotal():a.name,group:a,subTotal:!0});0===a.level&&this.columns.push(b);Ext.apply(b,{key:a.key,xexpandable:!0,xgrouped:!0});return b},parseAggregateForColumn:function(a,b){var c=[],d={},d=this.aggregate.getRange(),e=d.length,f,g;for(f=0;f<e;f++)g=d[f],this.internalCounter++,this.model.push({name:"c"+this.internalCounter,type:"auto",defaultValue:void 0,useNull:!0,col:a?a.key:this.grandTotalKey,agg:g.getId()}),c.push({dataIndex:"c"+this.internalCounter,text:g.header,topAxis:!0,subTotal:b?!0===b.subTotal:!1,grandTotal:b?!0===b.grandTotal:!1,dimension:g}),this.modelInfo["c"+this.internalCounter]=this.getDefaultFieldInfo({isColGroupTotal:b?!0===b.subTotal:!1,isColGrandTotal:b?!0===b.grandTotal:!1,topAxisColumn:!0,topAxisKey:a?a.key:this.grandTotalKey});0==c.length&&0==this.aggregate.getCount()?(this.internalCounter++,d=Ext.apply({text:a?a.name:"",dataIndex:"c"+this.internalCounter},b||{})):1==c.length?(d=Ext.applyIf({text:a?a.name:""},c[0]),Ext.apply(d,b||{}),b&&b.grandTotal&&1==this.aggregate.getCount()&&(d.text=this.aggregate.getAt(0).header||b.text)):d=Ext.apply({text:a?a.name:"",columns:c},b||{});return d},buildPivotStore:function(){Ext.isFunction(this.pivotStore.model.setFields)?this.pivotStore.model.setFields(this.model):this.pivotStore.model.replaceFields(this.model,!0);this.pivotStore.removeAll(!0);Ext.Array.each(this.leftAxis.getTree(),this.addRecordToPivotStore,this);this.addGrandTotalsToPivotStore()},addGrandTotalsToPivotStore:function(){var a=[],b,c,d;a.push({title:this.textGrandTotalTpl,values:this.preparePivotStoreRecordData({key:this.grandTotalKey},{isRowGrandTotal:!0})});if(Ext.isFunction(this.onBuildTotals))this.onBuildTotals(a);this.fireEvent("buildtotals",this,a);b=a.length;for(c=0;c<b;c++)d=a[c],Ext.isObject(d)&&Ext.isObject(d.values)&&(Ext.applyIf(d.values,{isRowGrandTotal:!0}),this.totals.push({title:d.title||"",record:this.pivotStore.add(d.values)[0]}))},addRecordToPivotStore:function(a){var b,c;if(a.children)a.records={},c="outline"==this.viewLayoutType?a.dimensionId:this.compactViewKey,a.records.collapsed=this.pivotStore.add(this.preparePivotStoreRecordData(a,{isRowGroupHeader:!0,isRowGroupTotal:!0}))[0],"first"==this.rowSubTotalsPosition?a.records.expanded=this.pivotStore.add(this.preparePivotStoreRecordData(a,{isRowGroupHeader:!0}))[0]:(b={},b[c]=a.name,b.isRowGroupHeader=!0,a.records.expanded=this.pivotStore.add(b)[0],"last"==this.rowSubTotalsPosition&&(b=this.preparePivotStoreRecordData(a,{isRowGroupTotal:!0}),b[c]=a.getTextTotal(),a.records.footer=this.pivotStore.add(b)[0])),Ext.Array.each(a.children,this.addRecordToPivotStore,this);else{b=this.pivotStore.add(this.preparePivotStoreRecordData(a))[0];a.record=b;if(Ext.isFunction(this.onBuildRecord))this.onBuildRecord(b,a);this.fireEvent("recordbuilt",this,b,a)}},preparePivotStoreRecordData:function(a,b){var c={},d=this.model.length,e,f,g;if(a){a.dimensionId&&(c[a.dimensionId]=a.name);c.leftAxisKey=a.key;for(e=0;e<d;e++)f=this.model[e],f.col&&f.agg&&(g=this.results.items.map[a.key+"/"+f.col],c[f.name]=g?g.values[f.agg]:null);"compact"==this.viewLayoutType&&(c[this.compactViewKey]=a.name)}return Ext.applyIf(c,b)},getColumns:function(){return this.model},getColumnHeaders:function(){this.model||this.buildModelAndColumns();return this.columns},isGroupRow:function(a){return(a=this.leftAxis.findTreeElement("key",a))?a.node.children&&0==a.node.children.length?0:a.level:!1},isGroupCol:function(a){return(a=this.topAxis.findTreeElement("key",a))?a.node.children&&0==a.node.children.length?0:a.level:!1},deprecated:{"6.0":{properties:{mztype:"type",mztypeLeftAxis:"leftAxisType",mztypeTopAxis:"topAxisType"}}}});Ext.define("Ext.pivot.matrix.Local",{alternateClassName:["Mz.aggregate.matrix.Local"],extend:"Ext.pivot.matrix.Base",alias:"pivotmatrix.local",requires:["Ext.pivot.matrix.Base","Ext.pivot.axis.Local","Ext.pivot.result.Local"],resultType:"local",leftAxisType:"local",topAxisType:"local",store:null,recordsPerJob:1E3,timeBetweenJobs:2,onInitialize:function(){this.localDelayedTask=new Ext.util.DelayedTask(this.delayedProcess,this);this.recordsToAddDelayedTask=new Ext.util.DelayedTask(this.onOriginalStoreAddDelayed,this);this.recordsToUpdateDelayedTask=new Ext.util.DelayedTask(this.onOriginalStoreUpdateDelayed,this);this.recordsToRemoveDelayedTask=new Ext.util.DelayedTask(this.onOriginalStoreRemoveDelayed,this);this.initializeStore({store:this.store});this.callParent(arguments)},initializeStore:function(a){var b,c;this.processedRecords={};a.store?c=a.store:this.store&&(this.store.isStore&&!this.storeListeners?b=this.store:c=this.store);c&&(b=Ext.getStore(c||""),Ext.isEmpty(b)&&Ext.isString(c)&&(b=Ext.create(c)));b&&b.isStore&&(Ext.destroy(this.storeListeners),this.store&&this.store.autoDestroy&&b!=this.store&&Ext.destroy(this.store),this.store=b,this.storeListeners=this.store.on({refresh:this.startProcess,beforeload:this.onOriginalStoreBeforeLoad,add:this.onOriginalStoreAdd,update:this.onOriginalStoreUpdate,remove:this.onOriginalStoreRemove,clear:this.startProcess,scope:this,destroyable:!0}),b.isLoaded()&&this.startProcess())},onReconfigure:function(a){this.initializeStore(a);this.callParent(arguments)},onDestroy:function(){this.localDelayedTask.cancel();this.localDelayedTask=null;this.recordsToAddDelayedTask.cancel();this.recordsToAddDelayedTask=null;this.recordsToUpdateDelayedTask.cancel();this.recordsToUpdateDelayedTask=null;this.recordsToRemoveDelayedTask.cancel();this.recordsToRemoveDelayedTask=null;Ext.isArray(this.records)&&(this.records.length=0);this.records=this.recordsToAdd=this.recordsToUpdate=this.recordsToRemove=null;Ext.destroy(this.storeListeners);this.store&&this.store.isStore&&this.store.autoDestroy&&Ext.destroy(this.store);this.store=this.storeListeners=this.processedRecords=null;this.callParent(arguments)},onOriginalStoreBeforeLoad:function(a){this.fireEvent("start",this)},onOriginalStoreAdd:function(a,b){this.recordsToAdd=this.recordsToAdd||[];Ext.Array.insert(this.recordsToAdd,this.recordsToAdd.length,Ext.Array.from(b));this.recordsToAddDelayedTask.delay(100)},onOriginalStoreAddDelayed:function(){var a=[],b,c,d,e;d=Ext.Array.from(this.recordsToAdd||[]);b=d.length;for(c=0;c<b;c++)e=d[c],this.processRecord(e,c,b),e=this.processedRecords[e.internalId],e.left.length&&Ext.Array.insert(a,a.length,e.left);this.recordsToAdd=[];if(b=a.length)for(this.leftAxis.rebuildTree(),this.topAxis.rebuildTree(),c=0;c<b;c++)e=a[c],(e.children&&!e.records||!e.children&&!e.record)&&this.addRecordToPivotStore(e);this.recalculateResults(this.store,d,0<b)},onOriginalStoreUpdate:function(a,b){this.recordsToUpdate=this.recordsToUpdate||[];Ext.Array.insert(this.recordsToUpdate,this.recordsToUpdate.length,Ext.Array.from(b));this.recordsToUpdateDelayedTask.delay(100)},onOriginalStoreUpdateDelayed:function(){var a=[],b=this.leftAxis.dimensions.items,c=b.length,d,e,f,g,h,k,l;g=Ext.Array.from(this.recordsToUpdate||[]);d=g.length;for(e=0;e<d;e++){h=g[e];k=!1;for(f=0;f<c;f++)if(l=b[f],h.isModified(l.dataIndex)){k=!0;break}k&&(this.removeRecordFromResults(h),this.processRecord(h,e,d),f=this.processedRecords[h.internalId],f.left.length&&Ext.Array.insert(a,a.length,f.left))}this.recordsToUpdate=[];if(d=a.length)for(this.leftAxis.rebuildTree(),this.topAxis.rebuildTree(),e=0;e<d;e++)f=a[e],(f.children&&!f.records||!f.children&&!f.record)&&this.addRecordToPivotStore(f);this.recalculateResults(this.store,g,0<d)},onOriginalStoreRemove:function(a,b,c,d){d||(this.recordsToRemove=this.recordsToRemove||[],Ext.Array.insert(this.recordsToRemove,this.recordsToRemove.length,Ext.Array.from(b)),this.recordsToRemoveDelayedTask.delay(100))},onOriginalStoreRemoveDelayed:function(){var a,b,c,d;c=Ext.Array.from(this.recordsToRemove||[]);a=c.length;for(b=0;b<a;b++)d=this.removeRecordFromResults(c[b])||d;this.recordsToRemove=[];d&&(this.leftAxis.rebuildTree(),this.topAxis.rebuildTree());this.recalculateResults(this.store,c,d)},removeRecordFromResults:function(a){var b=this.processedRecords[a.internalId],c=this.grandTotalKey,d=!1,e,f,g,h,k;if(!b)return d;if(e=this.results.get(c,c))e.removeRecord(a),0===e.records.length&&this.results.remove(c,c);h=b.top.length;for(g=0;g<h;g++)if(f=b.top[g],e=this.results.get(c,f.key))e.removeRecord(a),0===e.records.length&&(this.results.remove(c,f.key),this.topAxis.items.remove(f),d=!0);h=b.left.length;for(g=0;g<h;g++){f=b.left[g];if(e=this.results.get(f.key,c))e.removeRecord(a),0===e.records.length&&(this.results.remove(f.key,c),this.leftAxis.items.remove(f),d=!0);k=b.top.length;for(f=0;f<k;f++)if(e=this.results.get(b.left[g].key,b.top[f].key))e.removeRecord(a),0===e.records.length&&this.results.remove(b.left[g].key,b.top[f].key)}return d},recalculateResults:function(a,b,c){this.fireEvent("beforeupdate",this,c);this.buildModelAndColumns();this.results.calculate();Ext.Array.each(this.leftAxis.getTree(),this.updateRecordToPivotStore,this);this.updateGrandTotalsToPivotStore();this.fireEvent("afterupdate",this,c)},updateGrandTotalsToPivotStore:function(){var a=[],b;if(!(0>=this.totals.length)){a.push({title:this.textGrandTotalTpl,values:this.preparePivotStoreRecordData({key:this.grandTotalKey})});if(Ext.isFunction(this.onBuildTotals))this.onBuildTotals(a);this.fireEvent("buildtotals",this,a);if(this.totals.length===a.length)for(b=0;b<this.totals.length;b++)Ext.isObject(a[b])&&Ext.isObject(a[b].values)&&this.totals[b].record instanceof Ext.data.Model&&(delete a[b].values.id,this.totals[b].record.set(a[b].values))}},updateRecordToPivotStore:function(a){var b,c;a.children?(a.records&&(b="outline"==this.viewLayoutType?a.dimensionId:this.compactViewKey,c=this.preparePivotStoreRecordData(a),delete c.id,delete c[b],a.records.collapsed.set(c),"first"==this.rowSubTotalsPosition?a.records.expanded.set(c):"last"==this.rowSubTotalsPosition&&a.records.footer.set(c)),Ext.Array.each(a.children,this.updateRecordToPivotStore,this)):a.record&&(c=this.preparePivotStoreRecordData(a),delete c.id,a.record.set(c))},startProcess:function(){!this.store||this.store&&!this.store.isStore||this.isDestroyed||this.store.isLoading()||(this.clearData(),this.localDelayedTask.delay(50))},delayedProcess:function(){this.fireEvent("start",this);this.records=this.store.getRange();0==this.records.length?this.endProcess():(this.statusInProgress=!1,this.processRecords(0))},processRecords:function(a){var b=a,c;if(!this.isDestroyed){c=this.records.length;for(this.statusInProgress=!0;b<c&&b<a+this.recordsPerJob&&this.statusInProgress;)this.processRecord(this.records[b],b,c),b++;b>=c?(this.statusInProgress=!1,this.results.calculate(),this.leftAxis.buildTree(),this.topAxis.buildTree(),this.filterApplied&&this.results.calculate(),this.records=null,this.endProcess()):this.statusInProgress&&0<c&&Ext.defer(this.processRecords,this.timeBetweenJobs,this,[b])}},processRecord:function(a,b,c){var d=this.grandTotalKey,e,f,g,h,k,l,m;this.processedRecords[a.internalId]=l={left:[],top:[]};e=this.leftAxis.processRecord(a);f=this.topAxis.processRecord(a);if(e&&f){this.results.add(d,d).addRecord(a);k=f.length;for(g=0;g<k;g++)m=f[g],this.topAxis.addItem(m),l.top.push(this.topAxis.items.map[m.key]),this.results.add(d,m.key).addRecord(a);c=e.length;for(g=0;g<c;g++)for(m=e[g],this.leftAxis.addItem(m),l.left.push(this.leftAxis.items.map[m.key]),this.results.add(m.key,d).addRecord(a),h=0;h<k;h++)this.results.add(m.key,f[h].key).addRecord(a)}this.fireEvent("progress",this,b+1,c)},getRecordsByRowGroup:function(a){a=this.results.getByLeftKey(a);var b=a.length,c=[],d;for(d=0;d<b;d++)Ext.Array.insert(c,c.length,a[d].records||[]);return c},getRecordsByColGroup:function(a){a=this.results.getByTopKey(a);var b=a.length,c=[],d;for(d=0;d<b;d++)Ext.Array.insert(c,c.length,a[d].records||[]);return c},getRecordsByGroups:function(a,b){var c=this.results.get(a,b);return c?c.records||[]:[]}});Ext.define("Ext.pivot.matrix.Remote",{alternateClassName:["Mz.aggregate.matrix.Remote"],extend:"Ext.pivot.matrix.Base",alias:"pivotmatrix.remote",url:"",timeout:3E3,onBeforeRequest:Ext.emptyFn,onRequestException:Ext.emptyFn,onInitialize:function(){this.remoteDelayedTask=new Ext.util.DelayedTask(this.delayedProcess,this);this.callParent(arguments)},onDestroy:function(){this.remoteDelayedTask.cancel();this.remoteDelayedTask=null;this.callParent()},startProcess:function(){Ext.isEmpty(this.url)||(this.clearData(),this.fireEvent("start",this),this.statusInProgress=!1,this.remoteDelayedTask.delay(5))},delayedProcess:function(){var a=this.serialize(),b;b={keysSeparator:this.keysSeparator,grandTotalKey:this.grandTotalKey,leftAxis:a.leftAxis,topAxis:a.topAxis,aggregate:a.aggregate};a=this.fireEvent("beforerequest",this,b);!1!==a&&Ext.isFunction(this.onBeforeRequest)&&(a=this.onBeforeRequest(b));!1===a?this.endProcess():Ext.Ajax.request({url:this.url,timeout:this.timeout,jsonData:b,callback:this.processRemoteResults,scope:this})},processRemoteResults:function(a,b,c){var d=!b;a=Ext.JSON.decode(c.responseText,!0);var e;b&&(d=!a||!a.success);if(d){if(this.fireEvent("requestexception",this,c),Ext.isFunction(this.onRequestException))this.onRequestException(c)}else{b=Ext.Array.from(a.leftAxis||[]);d=b.length;for(e=0;e<d;e++)c=b[e],Ext.isObject(c)&&this.leftAxis.addItem(c);b=Ext.Array.from(a.topAxis||[]);d=b.length;for(e=0;e<d;e++)c=b[e],Ext.isObject(c)&&this.topAxis.addItem(c);b=Ext.Array.from(a.results||[]);d=b.length;for(e=0;e<d;e++)c=b[e],Ext.isObject(c)&&(a=this.results.add(c.leftKey||"",c.topKey||""),Ext.Object.each(c.values||{},a.addValue,a))}this.endProcess()}});Ext.define("Ext.pivot.Grid",{extend:"Ext.grid.Panel",alternateClassName:["Mz.pivot.Grid","Mz.pivot.Table"],xtype:["pivotgrid","mzpivotgrid"],requires:["Ext.pivot.matrix.Local","Ext.pivot.matrix.Remote","Ext.pivot.feature.PivotView","Ext.data.ArrayStore"],subGridXType:"gridpanel",isPivotGrid:!0,isPivotComponent:!0,enableLoadMask:!0,enableLocking:!1,enableColumnSort:!0,columnLines:!0,clsGroupTotal:Ext.baseCSSPrefix+"pivot-grid-group-total",clsGrandTotal:Ext.baseCSSPrefix+"pivot-grid-grand-total",startRowGroupsCollapsed:!0,startColGroupsCollapsed:!0,stateEvents:["pivotgroupexpand","pivotgroupcollapse","pivotdone"],groupHeaderCollapsedCls:Ext.baseCSSPrefix+"pivot-grid-group-header-collapsed",groupHeaderCollapsibleCls:Ext.baseCSSPrefix+"pivot-grid-group-header-collapsible",groupCls:Ext.baseCSSPrefix+"pivot-grid-group",relayedMatrixEvents:"beforereconfigure reconfigure start progress done modelbuilt columnsbuilt recordbuilt buildtotals storebuilt groupexpand groupcollapse beforerequest requestexception".split(" "),deprecatedConfigs:"matrixConfig leftAxis topAxis aggregate showZeroAsBlank textTotalTpl textGrandTotalTpl viewLayoutType rowSubTotalsPosition rowGrandTotalsPosition colSubTotalsPosition colGrandTotalsPosition".split(" "),config:{matrix:{type:"local"}},initComponent:function(){this.columns=[];this.preInitialize();this.callParent(arguments);this.postInitialize()},preInitialize:function(){this.features=[{id:"group",ftype:"pivotview",summaryRowCls:this.clsGroupTotal,grandSummaryRowCls:this.clsGrandTotal}];this.addCls(Ext.baseCSSPrefix+"pivot-grid");this.store&&(this.originalStore=this.store);this.store=new Ext.data.ArrayStore({fields:[]});this.enableColumnMove=!1},postInitialize:function(){var a={headerclick:this.onHeaderClick,scope:this,destroyable:!0},b=this.getView(),c=[],d=this.deprecatedConfigs,e=d.length,f=this.getMatrix();this.enableLocking?(this.lockedHeaderCtListeners=b.lockedView.getHeaderCt().on(a),this.headerCtListeners=b.normalView.getHeaderCt().on(a)):this.headerCtListeners=b.getHeaderCt().on(a);for(a=0;a<e;a++)b=d[a],this[b]&&c.push(b);if(0<c.length){d={};e=c.length;for(a=0;a<e;a++)b=c[a],"matrixConfig"===b?Ext.apply(d,this.matrixConfig):d[b]=this[b];this.originalStore&&(d.store=this.originalStore);if(f){if(d.type&&f.type===d.type){f.reconfigure(d);return}}else d.type||(d.type="local"),d.cmp=this;this.setMatrix(d)}},destroy:function(){this.setMatrix(null);Ext.destroy(this.headerCtListeners,this.lockedHeaderCtListeners);Ext.destroy(this.originalStore);this.originalStore=this.pivotColumns=this.headerCtListeners=this.lockedHeaderCtListeners=null;this.callParent(arguments);Ext.destroy(this.store);this.store=null},applyMatrix:function(a,b){Ext.destroy(b);if(null==a)return a;if(a&&a.isPivotMatrix)return a.cmp=this,a;Ext.applyIf(a,{type:"local"});a.cmp=this;"local"==a.type&&this.originalStore&&Ext.applyIf(a,{store:this.originalStore});return Ext.Factory.pivotmatrix(a)},updateMatrix:function(a,b){this.matrixRelayedListeners=this.matrixListeners=Ext.destroy(b,this.matrixListeners,this.matrixRelayedListeners);a&&(this.matrixListeners=this.matrix.on({cleardata:this.onMatrixClearData,start:this.onMatrixProcessStart,progress:this.onMatrixProcessProgress,done:this.onMatrixDataReady,afterupdate:this.onMatrixAfterUpdate,groupexpand:this.onMatrixGroupExpandCollapse,groupcollapse:this.onMatrixGroupExpandCollapse,scope:this,destroyable:!0}),this.matrixRelayedListeners=this.relayEvents(this.matrix,this.relayedMatrixEvents,"pivot"))},refreshView:function(){this.destroyed||this.destroying||this.store.fireEvent("pivotstoreremodel",this)},updateHeaderContainerColumns:function(a){var b=this.getView(),c=b.normalView?b.normalView.getHeaderCt():b.getHeaderCt(),b=b.normalGrid?b.normalGrid:this,d=0,e,f;if(a){if(e=this.getColumnForGroup(c.items,a),e.found){b.reconfiguring=!0;c=e.item.ownerCt;for(e=e.index;d<c.items.length;)f=c.items.getAt(d),f.group==a?Ext.destroy(f):d++;d=Ext.clone(this.pivotColumns);this.preparePivotColumns(d);d=this.getVisiblePivotColumns(this.prepareVisiblePivotColumns(d),a);d=c.insert(e,d);b.reconfiguring=!1;d&&d.length&&d[0].focus();b.onHeadersChanged()}}else d=Ext.clone(this.pivotColumns),this.preparePivotColumns(d),d=this.prepareVisiblePivotColumns(d),this.reconfigure(void 0,d)},getColumnForGroup:function(a,b){var c=a.length,d={found:!1,index:-1,item:null},e,f;for(e=0;e<c&&(f=a.getAt(e),f.group==b?(d.found=!0,d.index=e,d.item=f):f.items&&(d=this.getColumnForGroup(f.items,b)),!d.found);e++);return d},onMatrixClearData:function(){this.store.removeAll(!0);this.expandedItemsState||(this.lastColumnsState=null);this.sortedColumn=null},onMatrixProcessStart:function(){this.enableLoadMask&&this.setLoading(!0)},onMatrixProcessProgress:function(a,b,c){var d;this.loadMask&&(this.loadMask.msgTextEl?d=this.loadMask.msgTextEl:this.loadMask.msgEl&&(d=this.loadMask.msgEl),d&&d.update(Ext.util.Format.number(100*(b||.1)/(c||.1),"0")+"%"))},onMatrixDataReady:function(a){this.refreshMatrixData(a,!1)},onMatrixAfterUpdate:function(a,b){b&&this.refreshMatrixData(a,!0)},onMatrixGroupExpandCollapse:function(a,b,c){"col"==b&&this.updateHeaderContainerColumns(c)},refreshMatrixData:function(a,b){var c=a.getColumnHeaders(),d=!1,e=a.leftAxis.items.items,f=a.topAxis.items.items,g,h,k;this.enableLoadMask&&this.setLoading(!1);if(!b)if(this.expandedItemsState){h=e.length;for(g=0;g<h;g++)k=e[g],0<=Ext.Array.indexOf(this.expandedItemsState.rows,k.value)&&(d=k.expanded=!0);h=f.length;for(g=0;g<h;g++)k=f[g],0<=Ext.Array.indexOf(this.expandedItemsState.cols,k.value)&&(d=k.expanded=!0);d&&delete this.expandedItemsState}else this.doExpandCollapseTree(a.leftAxis.getTree(),!this.startRowGroupsCollapsed),this.doExpandCollapseTree(a.topAxis.getTree(),!this.startColGroupsCollapsed);this.pivotColumns=Ext.clone(c);c=Ext.clone(this.pivotColumns);this.preparePivotColumns(c);this.restorePivotColumnsState(c);c=this.prepareVisiblePivotColumns(c);this.reconfigure(void 0,c);Ext.isEmpty(this.sortedColumn)||a.leftAxis.sortTreeByField(this.sortedColumn.dataIndex,this.sortedColumn.direction);this.store.fireEvent("pivotstoreremodel",this);Ext.isEmpty(this.sortedColumn)||this.updateColumnSortState(this.sortedColumn.dataIndex,this.sortedColumn.direction)},getVisiblePivotColumns:function(a,b){var c=[],d=a.length,e,f;for(e=0;e<d;e++)f=a[e],f.group==b&&c.push(f),f.columns&&(c=Ext.Array.merge(c,this.getVisiblePivotColumns(f.columns,b)));return c},prepareVisiblePivotColumns:function(a){var b=a.length,c=[],d,e;for(d=0;d<b;d++)e=a[d],e.hidden||c.push(e),e.columns&&(e.columns=this.prepareVisiblePivotColumns(e.columns));return c},preparePivotColumns:function(a){var b={menuDisabled:!0,sortable:!1,lockable:!1},c=a.length,d,e;for(d=0;d<c;d++)e=a[d],e.cls=e.cls||"",Ext.apply(e,b),e.leftAxis&&(e.locked=this.enableLocking),e.subTotal&&(e.cls=e.tdCls=this.clsGroupTotal),e.grandTotal&&(e.cls=e.tdCls=this.clsGrandTotal),e.group&&e.xgrouped&&(e.group.expanded?e.subTotal||(e.cls+=(Ext.isEmpty(e.cls)?"":" ")+this.groupHeaderCollapsibleCls):e.subTotal&&(e.cls+=(Ext.isEmpty(e.cls)?"":" ")+this.groupHeaderCollapsibleCls+" "+this.groupHeaderCollapsedCls),e.subTotal?e.text=e.group.expanded?e.group.getTextTotal():Ext.String.format('\x3cspan class\x3d"'+this.groupCls+'"\x3e{0}\x3c/span\x3e',e.group.name):e.group&&(e.text=Ext.String.format('\x3cspan class\x3d"'+this.groupCls+'"\x3e{0}\x3c/span\x3e',e.group.name)),e.xexpandable=e.subTotal?!e.group.expanded:e.group.expanded,!e.group.expanded&&!e.subTotal||e.group.expanded&&e.subTotal&&"none"==this.getMatrix().colSubTotalsPosition)&&(e.hidden=!0),Ext.isEmpty(e.columns)?e.dimension&&(e.renderer=e.dimension?e.dimension.getRenderer():!1,e.formatter=e.dimension?e.dimension.getFormatter():!1,e.scope=e.dimension?e.dimension.scope:null,e.align=e.dimension.align,0<e.dimension.flex?e.flex=e.flex||e.dimension.flex:e.width=e.width||e.dimension.width):(e.focusable=!0,e.enableFocusableContainer=!0,this.preparePivotColumns(e.columns))},reconfigurePivot:function(a){var b=this.getMatrix();a=a||{};b?a.type&&b.type!==a.type?this.setMatrix(a):b.reconfigure(a):this.setMatrix(a)},doExpandCollapseTree:function(a,b){var c;for(c=0;c<a.length;c++)a[c].expanded=b,a[c].children&&this.doExpandCollapseTree(a[c].children,b)},doExpandCollapse:function(a,b,c,d){var e=this.getMatrix();e&&(a=("row"==a?e.leftAxis:e.topAxis).findTreeElement("key",b))&&(a=a.node,(c=Ext.isDefined(c)?c:!a.expanded)?a.expand(d):a.collapse(d))},setHeaderGroupVisibility:function(a){if(a.xgrouped&&(a.subTotal?(a.setText(a.group.expanded?a.group.getTextTotal():Ext.String.format('\x3cspan class\x3d"'+this.groupCls+'"\x3e{0}\x3c/span\x3e',a.group.name)),a.group.expanded?(a.removeCls(this.groupHeaderCollapsibleCls),a.removeCls(this.groupHeaderCollapsedCls)):(a.addCls(this.groupHeaderCollapsibleCls),a.addCls(this.groupHeaderCollapsedCls))):(a.setText(Ext.String.format('\x3cspan class\x3d"'+this.groupCls+'"\x3e{0}\x3c/span\x3e',a.group.name)),a.addCls(this.groupHeaderCollapsibleCls)),a.xexpandable=a.subTotal?!a.group.expanded:a.group.expanded,!a.group.expanded&&!a.subTotal||a.group.expanded&&a.subTotal&&"none"==this.getMatrix().colSubTotalsPosition)){a.hide();return}a.show();a.items.each(this.setHeaderGroupVisibility,this)},expandRow:function(a,b){this.doExpandCollapse("row",a,!0,b)},collapseRow:function(a,b){this.doExpandCollapse("row",a,!1,b)},expandCol:function(a,b){this.doExpandCollapse("col",a,!0,b)},collapseCol:function(a,b){this.doExpandCollapse("col",a,!1,b)},expandAll:function(){this.expandAllColumns();this.expandAllRows()},expandAllRows:function(){this.getMatrix().leftAxis.expandAll()},expandAllColumns:function(){this.getMatrix().topAxis.expandAll()},collapseAll:function(){this.collapseAllRows();this.collapseAllColumns()},collapseAllRows:function(){this.getMatrix().leftAxis.collapseAll()},collapseAllColumns:function(){this.getMatrix().topAxis.collapseAll()},setStore:function(a){var b=this.getMatrix();b&&"local"===b.type&&b.initializeStore({store:a})},getStore:function(){var a=this.getMatrix();return(a instanceof Ext.pivot.matrix.Local?a.store:this.originalStore)||this.store},getPivotStore:function(){return this.store},getTopAxisItem:function(a){var b=this.getMatrix(),c=b.getColumns(),d,e;if(!a)return null;for(e=0;e<c.length;e++)if(c[e].name===a.dataIndex){d=c[e].col;break}return Ext.isEmpty(d)?null:b.topAxis.items.getByKey(d)},getLeftAxisItem:function(a){var b=this.getView();if(!a)return null;b=b.normalView||b;b=b.getFeature("group");return b?(a=b.dataSource.storeInfo[a.internalId])?this.getMatrix().leftAxis.items.getByKey(a.leftKey):null:null},onHeaderClick:function(a,b,c){a=b.sortState?"ASC"==b.sortState?"DESC":"ASC":"ASC";c&&c.stopEvent();if(!b.xexpandable){if(!this.enableColumnSort)return;(b.leftAxis||b.topAxis)&&!Ext.isEmpty(b.dataIndex)&&this.getMatrix().leftAxis.sortTreeByField(b.dataIndex,a)&&(this.refreshView(),this.updateColumnSortState(b,a));return!1}this.doExpandCollapse("col",b.key);return!1},updateColumnSortState:function(a,b){Ext.isString(a)&&(a=this.down('[dataIndex\x3d"'+a+'"]'));a&&(a.setSortState(new Ext.util.Sorter({direction:b,property:"dummy"})),a.sortState=b,this.sortedColumn={dataIndex:a.dataIndex,direction:b})},getStateProperties:function(){return["enableColumnSort","sortedColumn","startRowGroupsCollapsed","startColGroupsCollapsed"]},applyState:function(a){var b=this.getStateProperties(),c=this.getMatrix(),d,e;for(d=0;d<b.length;d++)e=b[d],a[e]&&(this[e]=a[e]);a.expandedItems&&(this.expandedItemsState=a.expandedItems);this.lastColumnsState=a.pivotcolumns||{};c?c.reconfigure(a.matrix):this.setMatrix(a.matrix)},getState:function(){var a={},b=this.getStateProperties(),c=this.getMatrix(),d,e;e=b.length;for(d=0;d<e;d++)a[b[d]]=this[b[d]];a.expandedItems={cols:[],rows:[]};if(c){a.matrix=c.serialize();e=c.leftAxis.items.getCount();for(d=0;d<e;d++)b=c.leftAxis.items.getAt(d),b.expanded&&a.expandedItems.rows.push(b.value);e=c.leftAxis.dimensions.getCount();for(d=0;d<e;d++)b=c.leftAxis.dimensions.getAt(d),a.matrix.leftAxis[d].id=b.getId();e=c.topAxis.items.getCount();for(d=0;d<e;d++)b=c.topAxis.items.getAt(d),b.expanded&&a.expandedItems.cols.push(b.value)}a.pivotcolumns=this.getPivotColumnsState();return a},getPivotColumnsState:function(){var a,b;if(!this.lastColumnsState)for(b=this.getDataIndexColumns(this.getMatrix().getColumnHeaders()),this.lastColumnsState={},a=0;a<b.length;a++)b[a].dataIndex&&(this.lastColumnsState[b[a].dataIndex]={width:b[a].width,flex:b[a].flex||0});b=this.getView().getGridColumns();for(a=0;a<b.length;a++)b[a].dataIndex&&(this.lastColumnsState[b[a].dataIndex]={width:b[a].rendered?b[a].getWidth():b[a].width,flex:b[a].flex||0});return this.lastColumnsState},getDataIndexColumns:function(a){var b=[],c;for(c=0;c<a.length;c++)a[c].dataIndex?b.push(a[c].dataIndex):Ext.isArray(a[c].columns)&&(b=Ext.Array.merge(b,this.getDataIndexColumns(a[c].columns)));return b},restorePivotColumnsState:function(a){this.parsePivotColumnsState(this.getPivotColumnsState(),a)},parsePivotColumnsState:function(a,b){var c,d;if(b)for(d=0;d<b.length;d++){if(c=a[b[d].dataIndex])c.flex?b[d].flex=c.flex:c.width&&(b[d].width=c.width);this.parsePivotColumnsState(a,b[d].columns)}},deprecated:{"6.2":{properties:{matrixConfig:null,viewLayoutType:"outline",rowSubTotalsPosition:"first",rowGrandTotalsPosition:"last",colSubTotalsPosition:"last",colGrandTotalsPosition:"last",textTotalTpl:"Total ({name})",textGrandTotalTpl:"Grand total",showZeroAsBlank:!1,leftAxis:null,topAxis:null,aggregate:null}}}});Ext.define("Ext.pivot.update.Base",{extend:"Ext.mixin.Observable",alias:"pivotupdate.base",mixins:["Ext.mixin.Factoryable"],config:{leftKey:null,topKey:null,matrix:null,dataIndex:null,value:null},destroy:function(){Ext.asapCancel(this.updateTimer);this.setMatrix(null);this.callParent()},getResult:function(){var a=this.getMatrix();return a?a.results.get(this.getLeftKey(),this.getTopKey()):null},update:function(){var a=this;Ext.asapCancel(a.updateTimer);return new Ext.Promise(function(b,c){a.getMatrix()&&a.getDataIndex()||Ext.raise("Invalid configuration");var d=a.getResult();d?!1!==a.fireEvent("beforeupdate",a)?a.updateTimer=Ext.asap(a.onUpdate,a,[d,b,c]):c("Operation canceled!"):c("No Result found!")})},onUpdate:function(a,b,c){this.fireEvent("update",this);b(this)}});Ext.define("Ext.pivot.update.Increment",{extend:"Ext.pivot.update.Base",alias:"pivotupdate.increment",onUpdate:function(a,b,c){var d=this.getDataIndex(),e=parseFloat(this.getValue()),f=a.records,g,h;if(f)for(g=f.length,h=0;h<g;h++)f[h].set(d,f[h].get(d)+e);this.callParent([a,b,c])}});Ext.define("Ext.pivot.update.Overwrite",{extend:"Ext.pivot.update.Base",alias:"pivotupdate.overwrite",onUpdate:function(a,b,c){if(!this.destroyed){var d=this.getDataIndex(),e=this.getValue(),f=a.records,g,h;if(f)for(g=f.length,h=0;h<g;h++)f[h].set(d,e);this.callParent([a,b,c])}}});Ext.define("Ext.pivot.update.Percentage",{extend:"Ext.pivot.update.Base",alias:"pivotupdate.percentage",onUpdate:function(a,b,c){var d=this.getDataIndex(),e=parseFloat(this.getValue()),f=a.records,g,h;if(f)for(g=f.length,h=0;h<g;h++)f[h].set(d,Math.floor(f[h].get(d)*e/100));this.callParent([a,b,c])}});Ext.define("Ext.pivot.update.Uniform",{extend:"Ext.pivot.update.Base",alias:"pivotupdate.uniform",onUpdate:function(a,b,c){var d=this.getDataIndex(),e=a.records,f,g,h;if(e&&(f=e.length,0<f))for(h=parseFloat(this.getValue())/f,g=0;g<f;g++)e[g].set(d,h);this.callParent([a,b,c])}});