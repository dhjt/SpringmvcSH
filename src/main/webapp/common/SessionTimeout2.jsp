<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="refresh" content="5;url=<%=basePath%>">
<title>未获取用户信息</title>
<style type="text/css">
h3 {color: #336699;font: normal 20px 黑体;line-height: 50px;}
.content {
	padding: 5px 5px;
	/*border: 1px solid #5577AA;*/
	display: inline;
}
img {border: 0;}
body {padding-top: 150px;}
</style>
</head>
<body>
<div align="center" class="content">
<H2 align="center">对不起，您未登录或长时间未对系统操作，请重新登录。</H2>
<H2 align="center"><span id="jump">5</span> 秒钟后页面将自动返回登录页面...</H2>
</div>
<script type="text/javascript">
function countDown(secs){
	var jump = document.getElementById("jump");
    jump.innerHTML=secs;
    if(--secs>0)
     setTimeout("countDown("+secs+" )",1000);
}
countDown(5);
</script>
</body>
</html>