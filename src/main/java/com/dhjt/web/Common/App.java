package com.dhjt.web.Common;

import com.dhjt.web.Dao.HibernateDao;
import com.dhjt.web.util.SpringUtil;

public class App {
	public static final String DEFAULT_THEME = "neptune";
	private static final String DEFAULT_FONT_SIZE = "12";
	private static final String DEFAULT_LINE_COUNT = "20";
	private static final String DEFAULT_FONT_FAMILY = "黑体";

	public static String getFontSize() {
		return DEFAULT_FONT_SIZE;
	}

	public static String getLineCount() {
		return DEFAULT_LINE_COUNT;
	}

	public static String getThemeName() {
		return DEFAULT_THEME;
	}

	public static String getFontFamily() {
		return DEFAULT_FONT_FAMILY;
	}

	static {
		boolean isSystemDataBase = false;
//		SpringUtil.initSpring();
		System.out.println("系统数据加载完毕，正在启动系统...");
	}

	/**
	 * 取得Hibernate数据库操作通用接口
	 *
	 * @return
	 */
	public static HibernateDao getHibernateDao() {
		return (HibernateDao) SpringUtil.getContext().getBean("hibernateDao");
	}
}
