package com.dhjt.web.Common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * 控制器接口类
 * @author DHJT 2018年9月7日 上午10:22:13
 *
 */
@Controller
public class BaseController {

	@Autowired
	private  HttpServletRequest request;

	@Autowired
	private  HttpServletResponse response;

	private int start;// 起始记录
	private int limit;// 查询结果集的大小、每页条数
	private int page;// 第几页

	public int getStart() {
		String start = getRequest().getParameter("start");
		return Integer.valueOf(start);
	}

	public void setStart(final int start) {
		this.start = start;
	}

	public int getLimit() {
		String limit = getRequest().getParameter("limit");
		return Integer.parseInt(limit);
	}

	public void setLimit(final int limit) {
		this.limit = limit;
	}

	public Integer getPage() {
		String page = getRequest().getParameter("start");
		return Integer.valueOf(page);
	}

	public void setPage(int page) {
		this.page = page;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}
}
