package com.dhjt.web.Dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.ScrollableResults;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhjt.web.Bean.Book;
import com.dhjt.web.Common.App;
import com.dhjt.web.Dao.BookDao;
import com.dhjt.web.Dao.Page;

@Repository
public class BookDaoImpl implements BookDao {

	@Autowired
    private SessionFactory sessionFactory;

    @Override
	public boolean save(Book book) {
        sessionFactory.getCurrentSession().save(book);
        return true;
    }

    @Override
	public List<Book> getBooks() {
        List<Book> bookList = null;
        String sql = "from Book";
        Query query = sessionFactory.getCurrentSession().createQuery(sql);
        bookList = query.list();
        App.getHibernateDao().queryPage("from Book", 10, 1);
        return bookList;
    }

    @Override
    public Page getPage(int pageSize, int pageNo) {
    	String hql = "from Book";
    	Query query =  sessionFactory.getCurrentSession().createQuery(hql);
    	//得到滚动结果集
        ScrollableResults scroll = query.scroll();
        //滚动到最后一行
        scroll.last();
        int totalCount = scroll.getRowNumber() + 1;
        System.out.println("总计路数：" + totalCount);
System.out.println(pageSize+ "size-no" + pageNo);
        //设置分页位置
//        query.setFirstResult(0).setMaxResults(15);
//maxPageNo 从0开始
int maxPageNo = totalCount % pageSize == 0 ? (totalCount / pageSize) : totalCount / pageSize + 1;
int newPageNo = pageNo;
if (maxPageNo < pageNo) {
	newPageNo = maxPageNo-1;
}
//        query.setFirstResult(newPageNo * pageSize).setMaxResults(pageSize);
        query.setFirstResult(newPageNo * pageSize).setMaxResults(pageSize);

        System.out.println(query.list());
        Page page = new Page(query.list(), totalCount, pageSize, pageNo);
    	return page;
    }
}
