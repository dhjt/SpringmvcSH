package com.dhjt.web.Dao.Impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.dhjt.web.Bean.User;
import com.dhjt.web.Dao.UserDao;

import cn.hutool.crypto.digest.DigestAlgorithm;
import cn.hutool.crypto.digest.Digester;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public String checkLogin(String username, String password) {
		Digester md5 = new Digester(DigestAlgorithm.MD5);
		String digestHex = md5.digestHex(password);
		String hql = "from User where username='" + username + "' and password='" + digestHex + "'";
		Query query =  sessionFactory.getCurrentSession().createQuery(hql);
		List<User> list = query.list();
		if (list != null && list.size() != 0) {
			return "true";
		} else {
			return "error";
		}
	}

}
