package com.dhjt.web.Dao;

import java.util.List;

import com.dhjt.web.Bean.Book;

public interface BookDao {
	public boolean save(Book book);
	public List<Book> getBooks();
	public Page getPage(int pageSize, int pageNo);
}
