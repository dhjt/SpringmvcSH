package com.dhjt.web.Dao;

import java.util.ArrayList;
import java.util.List;

/**
 * 分页java bean
 */
public class Page {
	public final static int PAGESIZE = 30;
	private int pageSize = PAGESIZE;
	private int pageNo = 0;
	private int totalCount = 0;
	private List content = null;

	public Page() {
	}

	public Page(List items, int totalCount) {
		setTotalCount(totalCount);
		setContent(items);
	}

	public Page(List items, int totalCount, int pageSize, int pageNo) {
		setTotalCount(totalCount);
		setContent(items);
		setPageSize(pageSize);
		setPageNo(pageNo);
	}

	/**
	 * 根据页号，取得该页第一条记录的索引位置
	 *
	 * @return
	 */
//	public int getStartIndex() {
//		return (getPageNo() - 1) * getPageSize();
//	}
//
//	public void setStartIndex(int startIndex) {
//		if (totalCount <= 0)
//			this.pageNo = 0;
//		else if (startIndex >= totalCount)
//			this.pageNo = this.getPageCount();
//		else if (startIndex < 0)
//			this.pageNo = 0;
//		else {
//			this.pageNo = startIndex / pageSize;// indexes[startIndex /
//												// pageSize];
//		}
//
//	}

	/**
	 * 取类表内容
	 *
	 * @return Returns the content.
	 */
	public List getContent() {
		if (content == null)
			content = new ArrayList();
		return content;
	}

	/**
	 * 取每页记录数
	 *
	 * @return Returns the pageCount.
	 */
//	public int getPageCount() {
//		pageCount = totalCount / this.getPageSize();
//		if (totalCount % pageSize > 0)
//			pageCount++;
//		return pageCount;
//	}

	/**
	 * 取页号
	 *
	 * @return Returns the pageNo.
	 */
	public int getPageNo() {
		return pageNo;
	}

	/**
	 * 设置页号
	 *
	 * @param pageNo
	 *            The pageNo to set.
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	/**
	 * 取得每页记录数
	 *
	 * @return Returns the pageSize.
	 */
//	public int getPageSize() {
//		if (pageSize == 0) {
//			String tmp = App.getConfigValue("linenumber");// App.getConfigValue("page.perPageCount");
//			if (tmp == null)
//				pageSize = 20;
//			else
//				pageSize = Integer.parseInt(tmp);
//		}
//		return pageSize;
//	}

	/**
	 * 设置每页记录数
	 *
	 * @param pageSize
	 *            The pageSize to set.
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return Returns the totalCount.
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount
	 *            The totalCount to set.
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * 设置分页记录
	 *
	 * @param content
	 *            The content to set.
	 */
	public void setContent(List content) {
		this.content = content;
	}

	// ---------------------GeneralDaoJdbcAbstract需要的方法----------------------------------
	/**
	 * 获取分页最大行索引
	 *
	 * @return 最大行索引
	 */
	public int getMaxRowCount() {
		return pageNo == 0 ? pageSize : pageSize * pageNo;
	}

	/**
	 * 执行分页操作
	 *
	 * @param content
	 *            进行过最大行索引约束的结果集合
	 */
	public void processPages(List content) {
		setContent(content.subList(getFirstResult(), content.size()));
	}

	/**
	 * 获取分页的第一条记录索引
	 *
	 * @return 第一条记录索引
	 */
	public int getFirstResult() {
		return (pageNo == 0 ? 0 : pageNo - 1) * pageSize;
	}
}
