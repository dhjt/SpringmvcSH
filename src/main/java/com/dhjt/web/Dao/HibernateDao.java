package com.dhjt.web.Dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.orm.hibernate5.HibernateTemplate;

public class HibernateDao extends HibernateTemplate {
	/**
	 * 分页检索
	 *
	 * @param pageSize 每页记录数
	 * @param pageNo 页号
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Page queryPage(final String hql, final int pageSize, final int pageNo) {
		return (Page) this.execute(new HibernateCallback() {
			@Override
			public Page doInHibernate(Session session) throws HibernateException {
				Query query = session.createQuery(hql);
				int totalCount = query.list().size();
				// maxPageNo 从0开始
				int maxPageNo = totalCount % pageSize == 0 ? (totalCount / pageSize - 1) : totalCount / pageSize;
				int newPageNo = pageNo;
				if (maxPageNo < pageNo) {
					newPageNo = maxPageNo;
				}
				if (totalCount == 0) {
					return new Page(new ArrayList(), 0);
				} else {
					query.setFirstResult(newPageNo * pageSize).setMaxResults(pageSize);
					List items = query.list();
					return new Page(items, totalCount, pageSize, newPageNo);
				}
			}
		});
	}

	@SuppressWarnings("unchecked")
	public Page queryPage(final DetachedCriteria detachedCriteria, final int pageSize, final int pageNo) {
		return (Page) this.execute(new HibernateCallback() {
			@Override
			public Page doInHibernate(Session session) throws HibernateException {
				Criteria criteria = detachedCriteria.getExecutableCriteria(session);
				int totalCount = ((Integer) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
				// maxPageNo 从0开始
				int maxPageNo = totalCount % pageSize == 0 ? (totalCount / pageSize - 1) : totalCount / pageSize;
				int newPageNo = pageNo;
				if (maxPageNo < pageNo) {
					newPageNo = maxPageNo;
				}
				if (totalCount == 0) {
					return new Page(new ArrayList(), 0, pageSize, newPageNo + 1);
				} else {
					criteria.setProjection(null);
					List items = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
							.setFirstResult(newPageNo * pageSize).setMaxResults(pageSize).list();
					return new Page(items, totalCount, pageSize, newPageNo);
				}
			}
		});

	}
}
