package com.dhjt.web.Dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

/**
 * Hibernate通用的持久层接口
 * @author DHJT 2018年9月8日 下午2:33:09
 *
 * @param <T>
 */
public interface BaseDao<T> {
	/**
	 *  保存
     *
     * @param entity
     * @return oid
     */
    public Serializable save(T entity);
    public void update(T entity);
    public void saveOrUpdate(T entity);
    public void delete(T entity);
    public T findById(Serializable oid);
    public List<T> findAll();

    /**
     * 查找满足条件的总记录数
     *
     * @param detachedCriteria
     * @return
     */
    Integer findRecordNumByPage(DetachedCriteria detachedCriteria);
    /**
     * 向分页对象中设置记录
     *
     * @param detachedCriteria
     *            离线查询对象
     * @param startIndex
     *            开始索引
     * @param pageSize
     *            每页记录数
     * @return
     */
    List<T> findByPage(DetachedCriteria detachedCriteria, Integer startIndex, Integer pageSize);

    /**
     * 通过条件查询
     *
     * @param detachedCriteria
     * @return
     */
    List<T> findByCriteria(DetachedCriteria detachedCriteria);
    /**
     * 通用更新方法
     *
     * @param queryName
     *            命名查询的名字，在映射文件中定义
     * @param objects
     *            参数列表
     */
    public void executeUpdate(String queryName, Object... objects);
}
