package com.dhjt.web.Bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Tree {
	// 节点上的文本信息
	private String text;
	// 节点图标对应的路径
	private String icon;
	// 应用到节点图标上的样式
	private String iconCls;
	private String cls;
	private boolean checked;
	private boolean leaf;
	private Map<String, Object> attributes;
	private boolean click;
	private List<Tree> children = new ArrayList<Tree>();

	public boolean getChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public List<Tree> getChildren() {
		return children;
	}

	public void setChildren(List<Tree> children) {
		this.children = children;
	}

	public boolean isClick() {
		return click;
	}

	public void setClick(boolean click) {
		this.click = click;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public String getCls() {
		return cls;
	}

	public void setCls(String cls) {
		this.cls = cls;
	}
}
