package com.dhjt.web.Bean;
/**
 * ExtJS 中的树
 * @author DHJT 2018年9月6日 下午3:17:00
 *
 */
public class ExtTree extends Tree {
	// 节点上的提示信息
	private String qtip;

	public String getQtip() {
		return qtip;
	}

	public void setQtip(String qtip) {
		this.qtip = qtip;
	}

}
