package com.dhjt.web.Bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 角色Bean
 * @author DHJT 2018年9月7日 下午9:32:25
 *
 */
/** 角色 */
@Entity
@Table(name ="T_Role")
public class Role implements Serializable {
	@Id
	private String id;

	// 角色名
	@Column(name = "name")
	private String name;

	// 备注
	@Column(name = "REMARK")
	private String remark;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
