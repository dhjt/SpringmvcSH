package com.dhjt.web.Bean;

public class ExtCheckTree extends ExtTree {
	// 当前节点的选择状态
	private boolean checked;

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
}
