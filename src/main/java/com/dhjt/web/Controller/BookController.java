package com.dhjt.web.Controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dhjt.web.Bean.Book;
import com.dhjt.web.Common.BaseController;
import com.dhjt.web.Dao.Page;
import com.dhjt.web.service.BookService;

@Controller
public class BookController extends BaseController {

	@Autowired
    private BookService bookService;

    @RequestMapping(value = "save.do", method = RequestMethod.POST)
    public String save(HttpServletRequest req, HttpServletResponse resp) {
        String name = req.getParameter("name");
        Double price = Double.parseDouble(req.getParameter("price"));
        Book book = new Book();
        book.setName(name);
        book.setPrice(price);
        bookService.save(book);
        return list(req, resp);
    }

    @RequestMapping(value = "list.do", method = RequestMethod.GET)
    public String list(HttpServletRequest req, HttpServletResponse resp) {
        List<Book> bookList = bookService.getBooks();
        req.setAttribute("listBooks", bookList);
        return "view/book";
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public String getUserPage(@PathVariable("id") String id, Model model) {
    	System.out.println("2018年9月4日下午11:36:13 12-> " + id);
		Book book = new Book();
		book.setbId(1000L);
		book.setName("测试:" + id);
		book.setPrice(10.0);
		model.addAttribute(book);
		return "view/book";
    }

    @RequestMapping(value="{name}", method = RequestMethod.GET)
	public @ResponseBody Book getShopInJSON(@PathVariable String name) {
    	Book book = new Book();
    	book.setbId(100L);
    	book.setName("测试:" + name);
    	book.setPrice(12.0);
		return book;
	}

    @RequestMapping(value="/book/getBookList", method = RequestMethod.POST)
    public @ResponseBody Page getList() {
    	String limit = getRequest().getParameter("limit");
    	System.out.println("2018年9月6日下午7:19:50->" + limit);
    	Page page = new Page();
    	List<Book> bookList = bookService.getBooks();
    	page.setContent(bookList);
    	page.setTotalCount(bookList.size());
    	return page;
    }

	@RequestMapping(value="/book/getBookPage", method = RequestMethod.POST)
    public @ResponseBody Page getBookPage() {
//    	String limit = getRequest().getParameter("limit");
////    	int pageSize, int pageNo
//    	String pages = getRequest().getParameter("page");
//    	String start = getRequest().getParameter("start");
    	Page page = bookService.getPage(getLimit(), getPage());
    	return page;
    }
}
