package com.dhjt.web.Controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/index")
public class IndexController {

	String message = "Welcome to Spring MVC!";

    @RequestMapping(value="/hello.do")
    public ModelAndView getTest(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView("HelloWorld");
        return modelAndView;
    }

    @RequestMapping("/showMessage.do")
    public ModelAndView showMessage(@RequestParam(value = "name", required = false, defaultValue = "Spring") String name) {
    	System.out.println("2018年8月25日下午12:42:08->" + 121);
        ModelAndView mv = new ModelAndView("HelloWorld");//指定视图
        //向视图中添加所要展示或使用的内容，将在页面中使用
        mv.addObject("message", message);
        mv.addObject("name", name);
        return mv;
    }

//    @RequestMapping(value = "test", method = RequestMethod.GET)
}