package com.dhjt.web.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dhjt.web.Common.BaseController;
import com.dhjt.web.service.UserService;

/**
 * 登录处理的控制器
 * @author DHJT 2018年9月8日 下午2:02:33
 *
 */
@Controller
public class LoginController extends BaseController {

	@Autowired
	UserService userService;

	@RequestMapping(value = "/checkLogin", method = RequestMethod.POST)
	protected String checkLogin() {
		String username = getRequest().getParameter("username");
		String password = getRequest().getParameter("password");
		String massage = userService.checkLogin(username, password);
		if ("true".equalsIgnoreCase(massage)) {
			return "ExtDemo";
		}
		return "loginforExt";
	}
}
