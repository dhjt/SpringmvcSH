package com.dhjt.web.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dhjt.web.Common.BaseController;

@Controller
@RequestMapping("/tree")
public class TreeController extends BaseController {

	@RequestMapping(value = "/getTree", method = RequestMethod.GET)
	public String getTree(Model model) {
		String test = getRequest().getParameter("test");
		System.out.println(test);
		System.out.println("2018年9月4日下午6:06:23->" + "123412");
		List l = new ArrayList();
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("children", l);
		map.put("comment", "");
		map.put("icon", "dh-icon-button-jibenjiansuo");
		map.put("id", "16");
		map.put("sequence", 1);
		map.put("title", "基本检索");
		map.put("url", "Dh.search.BasicSearch");
		map.put("type", "JBJS");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("children", l);
		map.put("comment", "");
		map.put("icon", "dh-icon-button-yitihuajiansuo");
		map.put("id", "20");
		map.put("sequence", 2);
		map.put("title", "一体化检索");
		map.put("url", "Dh.search.LuceneQuery");
		map.put("type", "YTH");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("children", l);
		map.put("comment", "");
		map.put("icon", "dh-icon-button-jieyueshenpi");
		map.put("id", "21");
		map.put("sequence", 3);
		map.put("title", "登记管理");
		map.put("url", "Dh.borrow.ApprovalListGrid");
		map.put("type", "JYDJ");
		list.add(map);
		Map<String, Object> map2 = new HashMap<String, Object>();
		map2.put("children", list);
		map2.put("comment", "");
		map2.put("icon", "dh-icon-button-danganliyong");
		map2.put("id", "7");
		map2.put("sequence", 2);
		map2.put("title", "档案利用");
		map2.put("type", "DALY");
		map2.put("url", "");
		List<Map<String, Object>> list2 = new ArrayList<Map<String, Object>>();
		list2.add(map2);
		model.addAttribute(list2);
		return "book";
	}

	@ResponseBody
	public String getList(HttpServletResponse response) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("archiveType", "V");
		map.put("archiveTypeId", "3");
		map.put("id", "1501122252116");
		map.put("name", "文书档案");
		map.put("className", "FolderWS");
		list.add(map);
		map = new HashMap<String, Object>();
		map.put("archiveType", "F");
		map.put("archiveTypeId", "2");
		map.put("id", "1501122191996");
		map.put("name", "文书档案(一文一件)");
		map.put("className", "FileWS");
		list.add(map);
//		String jsonString = JSON.toJSONString(map);
		return "";
	}


}
