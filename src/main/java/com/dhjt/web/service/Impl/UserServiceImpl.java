package com.dhjt.web.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhjt.web.Dao.UserDao;
import com.dhjt.web.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
    private UserDao userDao;

	@Override
	@Transactional
	public String checkLogin(String username, String password) {
		return userDao.checkLogin(username, password);
	}

}
