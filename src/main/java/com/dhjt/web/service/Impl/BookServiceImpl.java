package com.dhjt.web.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dhjt.web.Bean.Book;
import com.dhjt.web.Dao.BookDao;
import com.dhjt.web.Dao.Page;
import com.dhjt.web.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
    private BookDao bookDao;

	@Override
	@Transactional
	public boolean save(Book book) {
		return bookDao.save(book);
	}

	@Override
	@Transactional
	public List<Book> getBooks() {
		return bookDao.getBooks();
	}

	@Override
	@Transactional
	public Page getPage(int pageSize, int pageNo) {
		return bookDao.getPage(pageSize, pageNo);
	}

}
