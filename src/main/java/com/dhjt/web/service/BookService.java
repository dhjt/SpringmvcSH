package com.dhjt.web.service;

import java.util.List;

import com.dhjt.web.Bean.Book;
import com.dhjt.web.Dao.Page;

public interface BookService {
	public boolean save(Book book);
	public List<Book> getBooks();
	public Page getPage(int pageSize, int pageNo);
}
