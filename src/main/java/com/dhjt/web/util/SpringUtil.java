package com.dhjt.web.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.support.TransactionTemplate;

import com.dhjt.web.Dao.HibernateDao;

public class SpringUtil {
	private static ApplicationContext springContext = null;
	/**
	 * Spring初始化
	 */
	public static synchronized void initSpring() {
		if (springContext == null) {
			System.out.println("Spring开始初始化...");
			try {
				springContext = new ClassPathXmlApplicationContext(
						"spring-context.xml");
				System.out.println("Spring成功初始化！");
				// configurer=(PropertyPlaceholderConfigurer)springContext.getBean("propertyConfigurer");
			} catch (BeansException e) {
				System.err.println("Spring初始化失败:" + e.toString());
				e.printStackTrace();
			}
		}
	}

	/**
	 * 取得SpringContent
	 *
	 * @return
	 */
	public static synchronized ApplicationContext getContext() {
		if (springContext == null) {
//			SpringUtil.initSpring();
		}
		return springContext;
	}

	/**
	 * 取得SpringContent定义的bean
	 *
	 * @param beanId
	 * @return
	 */
	public static synchronized Object getBean(String beanId) {
		return SpringUtil.getContext().getBean(beanId);
	}

	/**
	 * 取得Hibernate数据库操作通用接口
	 *
	 * @return
	 */
	public static HibernateDao getHibernateDao() {
		return (HibernateDao) SpringUtil.getContext().getBean("hibernateDao");
	}

	/**
	 * 取得jdbc数据库操作通用接口
	 *
	 * @return
//	 */
//	public static JdbcDao getJdbcDao() {
//		return (JdbcDao) SpringUtil.getContext().getBean("jdbcDao");
//	}

	public static TransactionTemplate getHibernateTransactionTemplate() {
		return (TransactionTemplate) SpringUtil.getContext().getBean(
				"hibernateTransactionTemplate");
	}

	public static TransactionTemplate getJdbcTransactionTemplate() {
		return (TransactionTemplate) SpringUtil.getContext().getBean(
				"jdbcTransactionTemplate");
	}

	/**
	 * 事物处理通用接口
	 *
	 * @param trans
	 * @throws TransactionException
	 */
//	public static void doInTransaction(final Transaction trans)
//			throws TransactionException {
//		App.getHibernateTransactionTemplate().execute(
//				new TransactionCallback() {
//					public Object doInTransaction(TransactionStatus ts) {
//						try {
//							trans.run();
//						} catch (Throwable e) {
//							ts.setRollbackOnly();
//							trans
//									.setTransactionException(new TransactionException(
//											e));
//							// e.printStackTrace();
//						}
//						return null;
//					}
//				});
//		TransactionException e = trans.getTransctionException();
//		if (e != null)
//			throw e;
//	}

	/**
	 * 事物处理通用接口
	 *
	 * @param trans
	 * @throws TransactionException
	 */
//	public static void doInTransaction(final Command cmd)
//			throws TransactionException {
//		final Map<String, TransactionException> map = new HashMap<String, TransactionException>();
//		App.getHibernateTransactionTemplate().execute(
//				new TransactionCallback() {
//					public Object doInTransaction(TransactionStatus ts) {
//						try {
//							cmd.run();
//						} catch (Throwable e) {
//							ts.setRollbackOnly();
//							map.put("EE", new TransactionException(e));
//							e.printStackTrace();
//						}
//						return null;
//					}
//				});
//		TransactionException ee = map.get("EE");
//		if (ee != null)
//			throw ee;
//	}
}
