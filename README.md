# SpringmvcSH
<!-- @author DHJT 2018-08-25 -->

<p align="center">
	<strong>A demo.</strong>
</p>
<p align="center">
	<a href="http://search.maven.org/#artifactdetails%7Ccn.hutool%7Chutool-all%7C4.1.10%7Cjar">
		<img src="https://img.shields.io/badge/version-0.3-blue.svg" >
	</a>
	<a href="https://mit-license.org/">
		<img src="http://img.shields.io/:license-mit-blue.svg" >
	</a>
	<a>
		<img src="https://img.shields.io/badge/JDK-1.8+-green.svg" >
	</a>
</p>
<p align="center">
	-- 主页：<a href="https://gitee.com/dhjt/SpringmvcSH">https://gitee.com/dhjt/SpringmvcSH/</a> --
</p>
<p align="center">
	-- 码云：<a href="http://gitee.com/dhjt">DHJT</a> --
</p>

#### 项目介绍
该项目是在Maven工具下建立的一个SpringMVC+Spring+Hibernate项目，可能生成各种版本和分支，用来改变项目的方向
参考链接：

- [JavaWeb之Eclipse中使用Maven构建SpringMVC项目](https://www.cnblogs.com/5ishare/p/7703513.html)
- [搭建 SSH（Spring+SpringMVC+Hibernate）框架](https://blog.csdn.net/u013447565/article/details/81880694)


#### 软件架构
- 工具说明:项目是在`Eclipse for JavaEE`工具下进行的建立以及后期的代码编写


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. [网站首页](http://127.0.0.1:8080/SpringmvcSH/index.jsp)
2. [获取Book实体的所有数据List、添加Book实体](http://127.0.0.1:8080/SpringmvcSH/list.do)
3. [ExtDemo](http://127.0.0.1:8080/SpringmvcSH/ExtDemo.jsp)
4. http://127.0.0.1:8080/SpringmvcSH/tree/getTree.json
5. http://127.0.0.1:8080/SpringmvcSH/book/getBookList

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)